import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class CollectionOperationsExecutor {

    private CollectionOperationsExecutor() {

    }

    private static boolean areTheSameType(Value value1, Value value2) {
        return (value1.isSet() && value2.isSet()) || (value1.isTuple() && value2.isTuple());
    }

    public static void include(Value leftValue, Value rightValue) {
        if(!leftValue.isCollection()) {
            throw new RuntimeException("Left value should be collection");
        }

        Collection<Value> collection;

        if(leftValue.isSet()) {
            collection = leftValue.asSet();
        }
        else {
            collection = leftValue.asTuple();
        }
        collection.add(rightValue);
    }

    public static Value extract(Value rightValue) {
        if(!rightValue.isCollection()) {
            throw new RuntimeException("Right value should be collection");
        }

        Value value;
        if(rightValue.isSet()) {
            Iterator<Value> iterator = rightValue.asSet().iterator();
            if(!iterator.hasNext()) return Value.VOID;
            value = iterator.next();
            iterator.remove();
        }
        else {
            if(rightValue.asTuple().isEmpty()) return Value.VOID;
            value = rightValue.asTuple().remove(rightValue.asTuple().size() - 1);
        }
        return value;
    }

    public static Value createSet(Value [] values) {
        return new Value(new HashSet<>(Arrays.asList(values)));
    }

    public static Value createList(Value [] values) {
        return new Value(new ArrayList<>(Arrays.asList(values)));
    }

    public static Value baseOperation(Value leftValue, Value rightValue, int opType) {
        if(!leftValue.isCollection() || !rightValue.isCollection()) {
            throw new RuntimeException("Only collections allow base set operations");
        }
        if(!areTheSameType(leftValue, rightValue)) {
            throw new RuntimeException("Collections types are not the same");
        }

        BaseCollectionOperationProvider provider = BaseCollectionOperationProvider.getInstance();

        if(leftValue.isSet()) {
            return new Value(provider.provide(opType)
                    .apply(leftValue.asSet(), rightValue.asSet())
                    .collect(Collectors.toSet()));
        }
        else {
            return new Value(provider.provide(opType)
                    .apply(leftValue.asTuple(), rightValue.asTuple())
                    .collect(Collectors.toList()));
        }
    }

    public static Value product(Value leftValue, Value rightValue) {
        if(!leftValue.isCollection() || (leftValue.isTuple() && !areTheSameType(leftValue, rightValue))) {
            throw new RuntimeException("Only sets allow multiplication operation");
        }
        return new Value(CollectionProcessor.cartesianProduct(leftValue.asSet(), rightValue.asSet()));
    }

    public static Value getTupleElement(Value tuple, Value idx) {
        if(!tuple.isTuple()) {
            throw new RuntimeException("tuple expected");
        }
        if(!idx.isNumber()) {
            throw new RuntimeException("index is not a number");
        }
        if(!idx.isInteger()) {
            idx = new Value(idx.asDouble().intValue());
        }

        if(idx.asInteger() >= tuple.asTuple().size() ||
                idx.asInteger() < 0) {
            throw new RuntimeException("Tuple index out of boundary");
        }

        return tuple.asTuple().get(idx.asInteger());
    }

    public static void replaceTupleElement(Value tuple, Value idx, Value newValue) {
        if(!tuple.isTuple()) {
            throw new RuntimeException("tuple expected");
        }

        if(!idx.isNumber()) {
            throw new RuntimeException("index is not a number");
        }
        if(!idx.isInteger()) {
            idx = new Value(idx.asDouble().intValue());
        }

        if(idx.asInteger() >= tuple.asTuple().size() ||
                idx.asInteger() < 0) {
            throw new RuntimeException("Tuple index out of boundary");
        }

        int targetIndex = idx.asInteger();
        tuple.asTuple().remove(targetIndex);
        tuple.asTuple().add(targetIndex, newValue);
    }

    public static Value getCardinality(Value collection) {
        if(collection.isSet()) return new Value(collection.asSet().size());
        return new Value(collection.asTuple().size());
    }

    public static Value copy(Value collection) {
        if(collection.isSet()) {
            return new Value(collection.asSet().stream().collect(Collectors.toSet()));
        }
        else if(collection.isTuple()) {
            return  new Value(collection.asTuple().stream().collect(Collectors.toList()));
        }
        else {
            return new Value(collection.getValue());
        }
    }


}