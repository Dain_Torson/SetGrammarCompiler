public class CastOperationsExecutor {

    private CastOperationsExecutor() {

    }

    public static Value castTo(Value value, String castExpr) {
        return CastOperationProvider.getInstance().provide(castExpr)
                .apply(value);
    }

    public static Value typeCheck(Value value, String checkExpr) {
        return TypeCheckOperationProvider.getInstance().provide(checkExpr)
                .apply(value);
    }
}