import java.util.HashMap;
import java.util.Objects;

public class BaseArithmeticOperationProvider {

    private static BaseArithmeticOperationProvider instance = new BaseArithmeticOperationProvider();
    private HashMap<Integer, Function2<Value, Value>> functionMap;

        private BaseArithmeticOperationProvider() {
            functionMap = new HashMap<>();
            initialize();
        }

        private void initialize() {
            functionMap.put(OPCodeStructure.PLUS, (arg1, arg2) -> new Value(arg1.asDouble() + arg2.asDouble()));
            functionMap.put(OPCodeStructure.MINUS, (arg1, arg2) -> new Value(arg1.asDouble() - arg2.asDouble()));
            functionMap.put(OPCodeStructure.MULT, (arg1, arg2) -> new Value(arg1.asDouble() * arg2.asDouble()));
            functionMap.put(OPCodeStructure.DIV, (arg1, arg2) -> new Value(arg1.asDouble() / arg2.asDouble()));
            functionMap.put(OPCodeStructure.GT, (arg1, arg2) -> new Value(arg1.asDouble() > arg2.asDouble()));
            functionMap.put(OPCodeStructure.LT, (arg1, arg2) -> new Value(arg1.asDouble() < arg2.asDouble()));
            functionMap.put(OPCodeStructure.GTEQ, (arg1, arg2) -> new Value(arg1.asDouble() >= arg2.asDouble()));
            functionMap.put(OPCodeStructure.LTEQ, (arg1, arg2) -> new Value(arg1.asDouble() <= arg2.asDouble()));
            functionMap.put(OPCodeStructure.EQ, (arg1, arg2) -> new Value(Objects.equals(arg1.asDouble(), arg2.asDouble())));
            functionMap.put(OPCodeStructure.NEQ, (arg1, arg2) -> new Value(!Objects.equals(arg1.asDouble(), arg2.asDouble())));
        }

        public Function2<Value, Value> provide(Integer type) {
            return functionMap.get(type);
        }

        public static BaseArithmeticOperationProvider getInstance() {
            return instance;
        }
}