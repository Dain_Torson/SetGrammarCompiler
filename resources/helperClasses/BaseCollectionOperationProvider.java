import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Stream;

public class BaseCollectionOperationProvider {

    private static BaseCollectionOperationProvider instance = new BaseCollectionOperationProvider();

    private HashMap<Integer, Function2<Collection<Value>, Stream<Value>>> functionMap;

    private BaseCollectionOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put(OPCodeStructure.UNION, CollectionProcessor::union);
        functionMap.put(OPCodeStructure.INTER, CollectionProcessor::intersection);
        functionMap.put(OPCodeStructure.DIFF, CollectionProcessor::difference);
        functionMap.put(OPCodeStructure.SDIFF, CollectionProcessor::symDifference);
    }

    public Function2<Collection<Value>, Stream<Value>> provide(Integer type) {
        return functionMap.get(type);
    }

    public static BaseCollectionOperationProvider getInstance() {
        return instance;
    }
}