

public class ElementOperationsExecutor {

    private ElementOperationsExecutor() {

    }

    public static Value baseOperation(Value leftValue, Value rightValue, int opType) {
        if(!leftValue.isNumber() || !rightValue.isNumber()) {
            throw new RuntimeException("Only numbers allow arithmetic operations");
        }

        boolean bothInt = false;

        if(leftValue.isInteger() && rightValue.isInteger()) {
            bothInt = true;
        }

        if(leftValue.isInteger()) {
            leftValue = new Value(Double.valueOf(leftValue.asInteger()));
        }
        if(rightValue.isInteger()) {
            rightValue = new Value(Double.valueOf(rightValue.asInteger()));
        }

        Value result = BaseArithmeticOperationProvider.getInstance().provide(opType)
                .apply(leftValue, rightValue);

        if(bothInt) {
            result = new Value(result.asDouble().intValue());
        }
        return result;
    }

    public static Value unaryMinus(Value value) {
        if(!value.isNumber()) {
            throw new RuntimeException("Only numbers allow arithmetic operations");
        }
        if(value.isInteger()) {
            return new Value(-value.asInteger());
        }
        else {
            return new Value(-value.asDouble());
        }
    }

    public static Value compare(Value leftValue, Value rightValue, int opType) {
        if(!leftValue.isNumber() || !rightValue.isNumber()) {
            throw new RuntimeException("Number expected.");
        }

        if(leftValue.isInteger()) {
            leftValue = new Value(Double.valueOf(leftValue.asInteger()));
        }
        if(rightValue.isInteger()) {
            rightValue = new Value(Double.valueOf(rightValue.asInteger()));
        }

        return BaseArithmeticOperationProvider.getInstance().provide(opType)
                .apply(leftValue, rightValue);
    }

    public static Value boolBaseOperation(Value leftValue, Value rightValue, int opType) {
        if(!leftValue.isBoolean() || !rightValue.isBoolean()) {
            throw new RuntimeException("Boolean value expected");
        }

        return BaseBoolOperationProvider.getInstance().provide(opType)
                .apply(leftValue, rightValue);
    }

    public static Value negate(Value value) {
        if(!value.isBoolean()) {
            throw new RuntimeException("Boolean value expected");
        }
        return new Value(!value.asBoolean());
    }

    public static Value concat(Value leftValue, Value rightValue) {
        if(!leftValue.isString()) {
            leftValue = new Value(leftValue.toString());
        }
        if(!rightValue.isString()) {
            rightValue = new Value(rightValue.toString());
        }
        return new Value(leftValue.asString() + rightValue.asString());
    }
}