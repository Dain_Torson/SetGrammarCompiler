import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class CastProcessor {

    public static Value castToNumber(Value input) {
        if(input.isBoolean()) {
            return input.asBoolean() ? new Value(1) : new Value(0);
        } else if(input.isString()) {
            return new Value(Double.parseDouble(input.asString()));
        }
        else if(input.isNumber()){
            return new Value(input.getValue());
        }
        else {
            throw new RuntimeException("Invalid cast to number");
        }
    }

    public static Value castToBool(Value input) {
        if(input.isBoolean()) {
            return new Value(input.getValue());
        }
        else if(input.isString()) {
            return input.asString().length() > 0 ? new Value(true) : new Value(false);
        }
        else if(input.isNumber()) {
            double doubleVal = input.isDouble() ? input.asDouble() : input.asInteger();
            return doubleVal != 0 ? new Value(true) : new Value(false);
        }
        else {
            throw new RuntimeException("Invalid cast to boolean");
        }
    }

    public static Value castToString(Value input) {
        return new Value(input.toString());
    }

    public static Value castToList(Value input) {
        if(input.isCollection()) {
            if(input.isSet()) {
                return new Value(input.asSet().stream().collect(Collectors.toList()));
            }
            return new Value(input.getValue());
        }
        else {
            List<Value> list = new ArrayList<>();
            list.add(input);
            return new Value(list);
        }
    }

    public static Value castToSet(Value input) {
        if(input.isCollection()) {
            if(input.isTuple()) {
                return new Value(input.asTuple().stream().collect(Collectors.toSet()));
            }
            return new Value(input.getValue());
        }
        else {
            Set<Value> list = new HashSet<>();
            list.add(input);
            return new Value(list);
        }
    }

}
