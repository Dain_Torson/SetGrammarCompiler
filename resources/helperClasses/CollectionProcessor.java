import java.util.*;
import java.util.stream.Stream;

public final class CollectionProcessor {

    private CollectionProcessor() {

    }

    public static Stream<Value> union(Collection<Value> collection1, Collection<Value> collection2) {
        return Stream.concat(collection1.stream(),
                collection2.stream());
    }

    public static Stream<Value> intersection(Collection<Value> collection1, Collection<Value> collection2) {
        return collection1.stream().filter(collection2::contains);
    }

    public static Stream<Value> difference(Collection<Value> collection1, Collection<Value> collection2) {
        return collection1.stream().filter(value -> !collection2.contains(value));
    }

    public static Stream<Value> symDifference(Collection<Value> collection1, Collection<Value> collection2) {
        return Stream.concat(
                collection1.stream().filter(value -> !collection2.contains(value)),
                collection2.stream().filter(value -> !collection1.contains(value))
        );
    }

    public static Set<Value> cartesianProduct(Set<Value> set1, Set<Value> set2) {
        Set<Value> result = new HashSet<>();
        for(Value value : set1) {
            for(Value other : set2) {
                List<Value> list = new ArrayList<>();
                list.add(value);
                list.add(other);
                result.add(new Value(list));
            }
        }
        return result;
    }
}
