import java.util.HashMap;

public class BaseBoolOperationProvider {

    private static BaseBoolOperationProvider instance = new BaseBoolOperationProvider();
    private HashMap<Integer, Function2<Value, Value>> functionMap;

    private BaseBoolOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put(OPCodeStructure.AND, (arg1, arg2) -> new Value(arg1.asBoolean() && arg2.asBoolean()));
        functionMap.put(OPCodeStructure.OR, (arg1, arg2) -> new Value(arg1.asBoolean() || arg2.asBoolean()));
        functionMap.put(OPCodeStructure.EQ, (arg1, arg2) -> new Value(arg1.asBoolean() == arg2.asBoolean()));
        functionMap.put(OPCodeStructure.NEQ, (arg1, arg2) -> new Value(arg1.asBoolean() != arg2.asBoolean()));
    }

    public Function2<Value, Value> provide(Integer type) {
        return functionMap.get(type);
    }

    public static BaseBoolOperationProvider getInstance() {
        return instance;
    }
}