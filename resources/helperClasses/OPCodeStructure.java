public class OPCodeStructure {
    public static final int
            T__0=1, OR=2, AND=3, EQ=4, NEQ=5, GT=6, LT=7, GTEQ=8, LTEQ=9, PLUS=10,
            MINUS=11, MULT=12, DIV=13, NOT=14, INCLUDE=15, EXTRACT=16, CONCAT=17,
            UNION=18, INTER=19, DIFF=20, SDIFF=21;
}