grammar SetGrammar;

@header {
package com.dain_torson.allset.autogen;
}

parse : func_definition* struct_element* EOF;

func_definition : DEFINE ID OPAR (((arg ',')+ arg) || arg?) CPAR block;

arg
    : reference
    | ID
    ;

struct_element: stm | block | command;

block : OBRACE struct_element* CBRACE;

command : keyword SCOL;

stm
    : assignment
    | if_stm
    | while_loop
    | for_loop
    | log
    | input
    | inclusion
    | extraction
    | scope_stm
    | function_call_stm
    ;

assignment
    : assign_expr SCOL           #generalAssignment
    | tuple_element ASSIGN expr SCOL     #tupleElementAssignment
    ;

assign_expr : ID ASSIGN expr;

inclusion : set_expr INCLUDE expr SCOL;
extraction : ID EXTRACT set_expr SCOL;
scope_stm : EXTERNAL ID SCOL;

if_stm : IF condition_block (ELIF condition_block)* (ELSE block)?;
condition_block : OPAR bool_expr CPAR block;

while_loop : WHILE OPAR bool_expr CPAR block;
for_loop : FOR OPAR assign_expr SCOL bool_expr SCOL assign_expr CPAR block;

function_call_stm : function_call SCOL;
function_call : ID OPAR (((param ',')+ param) || param?) CPAR;

param
    : id
    | expr
    ;

cast : CAST_TYPE OPAR expr CPAR;
type_check : CHECK_TYPE OPAR expr CPAR;

keyword
    : BREAK     #breakKeyWord
    | CONTINUE  #continueKeyWord
    | RETURN expr? #returnKeyWord
    ;

expr
 : set_expr
 | number_expr
 | string_expr
 | bool_expr
 ;

set_expr
   : set_expr op=(UNION | INTER | DIFF | SDIFF) set_expr    #setBaseOpExpr
   | OPAR set_expr CPAR                                     #setBracketExpr
   | set_expr PROD set_expr                                 #prodExpr
   | set                                                    #setAtomExpr
   | function_call                                          #setAsReturnValue
   | cast                                                   #setCastExpr
   | tuple_element                                          #setAsElementExpr
   | id                                                     #setIdExpr
    ;

number_expr
    : number_expr op=(PLUS | MINUS | MULT | DIV) number_expr    #baseArithmeticExpr
    | MINUS number_expr                                         #unaryMinusExpr
    | OPAR number_expr CPAR                                     #numberBracketExpr
    | number_atom                                               #numberAtomExpr
    | set_cardinality                                           #setCardinality
    | function_call                                             #numberAsReturnValue
    | tuple_element                                             #numberAsElementExpr
    | cast                                                      #numberCastExpr
    | id                                                        #numberIdExpr
    ;

string_expr
    : string_expr CONCAT string_expr  #stringConcatExpr
    | string_atom                   #stringAtomExpr
    | cast                          #stringCastExpr
    | function_call                 #stringAsReturnValue
    | tuple_element                 #stringAsElementExpr
    | id                            #stringIdExpr
    ;

bool_expr
    : bool_expr op=(EQ | NEQ | AND | OR) bool_expr                  #boolBaseExpr
    | NOT bool_expr                                                 #boolNegationExpr
    | OPAR bool_expr CPAR                                           #boolBracketExpr
    | number_expr op=(GT | LT | GTEQ | LTEQ | EQ | NEQ) number_expr #numberCompExpr
    | bool_atom                                                     #boolAtomExpr
    | cast                                                          #boolCastExpr
    | type_check                                                    #boolTypeCheck
    | function_call                                                 #boolAsReturnValue
    | tuple_element                                                 #boolAsElementExpr
    | id                                                            #boolIdExpr
    ;

set
   : SQLBRACE (((element)',')* (element)) SQRBRACE       #notEmptySet
   | LT (((element)',')* (element)) GT                   #nonEmptyTuple
   | LT GT                                               #emptyTuple
   | SQLBRACE SQRBRACE                                   #emptySet
   ;

element
    : atom
    | set
    ;

tuple_element : id '[' number_expr ']';
set_cardinality : CARD OPAR set_expr CPAR;

id : ID;
reference : REF ID;

atom
    : number_atom
    | string_atom
    | bool_atom
    ;

number_atom
     : INT              #intAtom
     | FLOAT            #floatAtom
     ;

string_atom
    : STRING    #stringAtom
    ;

bool_atom
    : (TRUE | FALSE)      #boolAtom
    ;

log : LOG expr SCOL;
input : INPUT ID? SCOL;

OR : 'or';
AND : 'and';
EQ : '==';
NEQ : '!=';
GT : '>';
LT : '<';
GTEQ : '>=';
LTEQ : '<=';
PLUS : '+';
MINUS : '-';
MULT : '*';
DIV : '/';
NOT : '!';
INCLUDE : '->';
EXTRACT : '<-';
CONCAT : '++' ;

UNION : 'union';
INTER : 'inter';
DIFF : 'diff';
SDIFF : 'sdiff';
PROD : 'mult';
CARD: 'card';
REF : '&';

SCOL : ';';
ASSIGN : '=';
OPAR : '(';
CPAR : ')';
OBRACE : '{';
CBRACE : '}';
SQLBRACE : '[';
SQRBRACE : ']';


TRUE : 'true';
FALSE : 'false';
IF : 'if';
ELSE : 'else';
ELIF : 'elif';
WHILE : 'while';
FOR : 'for';
BREAK : 'break';
CONTINUE : 'continue';
RETURN : 'return' ;

LOG : 'log';
INPUT : 'inp';

EXTERNAL : 'extern';
DEFINE : 'def';

CAST_TYPE : 'to_number' | 'to_bool' | 'to_set' | 'to_string' | 'to_tuple';
CHECK_TYPE : 'is_number' | 'is_bool' | 'is_set' | 'is_string' | 'is_tuple';

ID
 : [a-zA-Z_] [a-zA-Z_0-9]*
 ;

INT
 : [0-9]+
 ;

FLOAT
 : [0-9]+ '.' [0-9]* 
 | '.' [0-9]+
 ;

STRING
 : '"' (~["\r\n] | '""')* '"'
 ;

COMMENT
 : '#' ~[\r\n]* -> skip
 ;

SPACE
 : [ \t\r\n] -> skip
 ;

