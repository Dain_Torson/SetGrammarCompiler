package com.dain_torson.allset.compiler;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ScopeManager {

    private LinkedList<Scope> scopes;
    private List<String> additionalVariables;
    private boolean returned = false;

    public ScopeManager() {
        scopes = new LinkedList<>();
        scopes.add(new Scope());
        additionalVariables = new ArrayList<>();

    }

    public Scope getLast() {
        return scopes.getLast();
    }

    public Scope removeLast() {
        return scopes.removeLast();
    }

    public int size() {
        return scopes.size();
    }

    public Scope get(int idx) {
        return scopes.get(idx);
    }

    public void add() {
        Scope scope = new Scope();
        additionalVariables.forEach(scope::addVariable);
        additionalVariables.clear();
        scopes.add(scope);
    }

    public void setAdditionalVariables(List<String> additionalVariables) {
        this.additionalVariables = additionalVariables;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public boolean isReturned() {
        return returned;
    }

    public void clear(){
        returned = false;
        scopes.clear();
        scopes.add(new Scope());
    }
}
