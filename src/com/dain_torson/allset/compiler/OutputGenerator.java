package com.dain_torson.allset.compiler;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import java.io.*;
import java.util.LinkedList;


public class OutputGenerator {

    private STGroup stGroup;
    private ST baseTemp;
    private LinkedList<ST> templateList;

    private static String HELPER_CLASS_PATH = "resources/helperClasses/";
    private static OutputGenerator instance = new OutputGenerator();

    private OutputGenerator() {
        stGroup = new STGroupFile("templates/AllSetTemplates.stg");
        baseTemp = stGroup.getInstanceOf("base");
        templateList = new LinkedList<>();
        templateList.add(baseTemp);
    }

    public static OutputGenerator getInstance() {
        return instance;
    }

    public void generate(String path) throws IOException{
        PrintWriter writer = new PrintWriter(path + "Main.java", "UTF-8");
        writer.println(baseTemp.render());
        writer.close();
        generateHelperClasses(path);
    }

    public void generateHelperClasses(String path) throws IOException{
        copyFile(HELPER_CLASS_PATH, path, "Value.java");
        copyFile(HELPER_CLASS_PATH, path, "OPCodeStructure.java");
        copyFile(HELPER_CLASS_PATH, path, "BaseCollectionOperationProvider.java");
        copyFile(HELPER_CLASS_PATH, path, "CollectionProcessor.java");
        copyFile(HELPER_CLASS_PATH, path, "Function2.java");
        copyFile(HELPER_CLASS_PATH, path, "CollectionOperationsExecutor.java");
        copyFile(HELPER_CLASS_PATH, path, "BaseArithmeticOperationProvider.java");
        copyFile(HELPER_CLASS_PATH, path, "BaseBoolOperationProvider.java");
        copyFile(HELPER_CLASS_PATH, path, "CastOperationProvider.java");
        copyFile(HELPER_CLASS_PATH, path, "CastProcessor.java");
        copyFile(HELPER_CLASS_PATH, path, "TypeCheckOperationProvider.java");
        copyFile(HELPER_CLASS_PATH, path, "ElementOperationsExecutor.java");
        copyFile(HELPER_CLASS_PATH, path, "CastOperationsExecutor.java");
    }

    private void copyFile(String sourcePath, String destPath, String fileName) throws IOException {
        File source = new File(sourcePath + fileName);
        File dest = new File(destPath + fileName);
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    public ST newBlockTemplate() {
        ST template = getTemp("block");
        templateList.add(template);
        return template;
    }

    public ST newWrapper() {
        ST template = getTemp("wrapper");
        templateList.add(template);
        return template;
    }

    public ST removeTemplate() {
        return templateList.removeLast();
    }

    public ST getLastTemplate() {
        return templateList.getLast();
    }

    public ST getTemp(String name) {
        return stGroup.getInstanceOf(name);
    }

    public ST getBaseTemp() {
        return baseTemp;
    }
}
