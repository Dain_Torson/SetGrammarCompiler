package com.dain_torson.allset.compiler;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Scope {
    private List<String> externals;
    private List<String> variables;

    public Scope() {
        externals = new ArrayList<>();
        variables = new ArrayList<>();
    }

    public void addVariable(String id) {
        variables.add(id);
    }

    public boolean containsVariable(String id) {
        return variables.contains(id);
    }

    public void addExternal(String id) {
        externals.add(id);
    }

    public boolean containsExternal(String id) {
        return externals.contains(id);
    }

    public List<String> getVariables() {
        return variables;
    }
}
