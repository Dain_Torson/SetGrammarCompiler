package com.dain_torson.allset;

import com.dain_torson.allset.autogen.SetGrammarLexer;
import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.autogen.SetGrammarVisitor;
import com.dain_torson.allset.compiler.OutputGenerator;
import com.dain_torson.allset.visitors.CompilerVisitor;
import com.dain_torson.allset.visitors.InterpreterVisitor;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Objects;

public class Main {

    private static boolean compile = false;
    private static String path = "";

    public static void main(String[] args) throws Exception {

        if(args.length == 0) {
            System.out.println("No input found");
            return;
        }

        SetGrammarLexer lexer = new SetGrammarLexer(new ANTLRFileStream(args[0]));
        SetGrammarParser parser = new SetGrammarParser(new CommonTokenStream(lexer));
        ParseTree tree = parser.parse();

        if(args.length > 2) {
            if(Objects.equals(args[1], "-c") ) {
                compile = true;
                path = args[2];
            }
        }

        SetGrammarVisitor visitor;
        if(compile) {
            visitor = new CompilerVisitor();
        }
        else  {
            visitor = new InterpreterVisitor();
        }
        visitor.visit(tree);

        if(compile) {
            OutputGenerator.getInstance().generate(path);
        }
    }
}
