// Generated from resources/SetGrammar.g4 by ANTLR 4.7

package com.dain_torson.allset.autogen;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SetGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SetGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(SetGrammarParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#func_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc_definition(SetGrammarParser.Func_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(SetGrammarParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#struct_element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_element(SetGrammarParser.Struct_elementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(SetGrammarParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommand(SetGrammarParser.CommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStm(SetGrammarParser.StmContext ctx);
	/**
	 * Visit a parse tree produced by the {@code generalAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGeneralAssignment(SetGrammarParser.GeneralAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tupleElementAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupleElementAssignment(SetGrammarParser.TupleElementAssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#assign_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_expr(SetGrammarParser.Assign_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#inclusion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusion(SetGrammarParser.InclusionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#extraction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtraction(SetGrammarParser.ExtractionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#scope_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScope_stm(SetGrammarParser.Scope_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#if_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stm(SetGrammarParser.If_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#condition_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_block(SetGrammarParser.Condition_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#while_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_loop(SetGrammarParser.While_loopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#for_loop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_loop(SetGrammarParser.For_loopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#function_call_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call_stm(SetGrammarParser.Function_call_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#function_call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_call(SetGrammarParser.Function_callContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParam(SetGrammarParser.ParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#cast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCast(SetGrammarParser.CastContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#type_check}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_check(SetGrammarParser.Type_checkContext ctx);
	/**
	 * Visit a parse tree produced by the {@code breakKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakKeyWord(SetGrammarParser.BreakKeyWordContext ctx);
	/**
	 * Visit a parse tree produced by the {@code continueKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueKeyWord(SetGrammarParser.ContinueKeyWordContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnKeyWord(SetGrammarParser.ReturnKeyWordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SetGrammarParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setCastExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetCastExpr(SetGrammarParser.SetCastExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetAsReturnValue(SetGrammarParser.SetAsReturnValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setIdExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetIdExpr(SetGrammarParser.SetIdExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetAtomExpr(SetGrammarParser.SetAtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prodExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProdExpr(SetGrammarParser.ProdExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetAsElementExpr(SetGrammarParser.SetAsElementExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetBracketExpr(SetGrammarParser.SetBracketExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setBaseOpExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetBaseOpExpr(SetGrammarParser.SetBaseOpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setCardinality}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetCardinality(SetGrammarParser.SetCardinalityContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberCastExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberCastExpr(SetGrammarParser.NumberCastExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpr(SetGrammarParser.UnaryMinusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAtomExpr(SetGrammarParser.NumberAtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAsElementExpr(SetGrammarParser.NumberAsElementExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code baseArithmeticExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBaseArithmeticExpr(SetGrammarParser.BaseArithmeticExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberBracketExpr(SetGrammarParser.NumberBracketExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberAsReturnValue(SetGrammarParser.NumberAsReturnValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberIdExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberIdExpr(SetGrammarParser.NumberIdExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAsReturnValue(SetGrammarParser.StringAsReturnValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringCastExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringCastExpr(SetGrammarParser.StringCastExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringIdExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringIdExpr(SetGrammarParser.StringIdExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtomExpr(SetGrammarParser.StringAtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringConcatExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringConcatExpr(SetGrammarParser.StringConcatExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAsElementExpr(SetGrammarParser.StringAsElementExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolCastExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolCastExpr(SetGrammarParser.BoolCastExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberCompExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberCompExpr(SetGrammarParser.NumberCompExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolNegationExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolNegationExpr(SetGrammarParser.BoolNegationExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolBaseExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolBaseExpr(SetGrammarParser.BoolBaseExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolIdExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolIdExpr(SetGrammarParser.BoolIdExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolBracketExpr(SetGrammarParser.BoolBracketExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAtomExpr(SetGrammarParser.BoolAtomExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAsReturnValue(SetGrammarParser.BoolAsReturnValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolTypeCheck}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolTypeCheck(SetGrammarParser.BoolTypeCheckContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAsElementExpr(SetGrammarParser.BoolAsElementExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notEmptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotEmptySet(SetGrammarParser.NotEmptySetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nonEmptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNonEmptyTuple(SetGrammarParser.NonEmptyTupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyTuple(SetGrammarParser.EmptyTupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code emptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptySet(SetGrammarParser.EmptySetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(SetGrammarParser.ElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#tuple_element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTuple_element(SetGrammarParser.Tuple_elementContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#set_cardinality}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSet_cardinality(SetGrammarParser.Set_cardinalityContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(SetGrammarParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#reference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReference(SetGrammarParser.ReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(SetGrammarParser.AtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code intAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntAtom(SetGrammarParser.IntAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floatAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatAtom(SetGrammarParser.FloatAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link SetGrammarParser#string_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringAtom(SetGrammarParser.StringAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolAtom}
	 * labeled alternative in {@link SetGrammarParser#bool_atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolAtom(SetGrammarParser.BoolAtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(SetGrammarParser.LogContext ctx);
	/**
	 * Visit a parse tree produced by {@link SetGrammarParser#input}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInput(SetGrammarParser.InputContext ctx);
}