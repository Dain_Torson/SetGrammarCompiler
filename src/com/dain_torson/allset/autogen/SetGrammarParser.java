// Generated from resources/SetGrammar.g4 by ANTLR 4.7

package com.dain_torson.allset.autogen;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SetGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, OR=2, AND=3, EQ=4, NEQ=5, GT=6, LT=7, GTEQ=8, LTEQ=9, PLUS=10, 
		MINUS=11, MULT=12, DIV=13, NOT=14, INCLUDE=15, EXTRACT=16, CONCAT=17, 
		UNION=18, INTER=19, DIFF=20, SDIFF=21, PROD=22, CARD=23, REF=24, SCOL=25, 
		ASSIGN=26, OPAR=27, CPAR=28, OBRACE=29, CBRACE=30, SQLBRACE=31, SQRBRACE=32, 
		TRUE=33, FALSE=34, IF=35, ELSE=36, ELIF=37, WHILE=38, FOR=39, BREAK=40, 
		CONTINUE=41, RETURN=42, LOG=43, INPUT=44, EXTERNAL=45, DEFINE=46, CAST_TYPE=47, 
		CHECK_TYPE=48, ID=49, INT=50, FLOAT=51, STRING=52, COMMENT=53, SPACE=54;
	public static final int
		RULE_parse = 0, RULE_func_definition = 1, RULE_arg = 2, RULE_struct_element = 3, 
		RULE_block = 4, RULE_command = 5, RULE_stm = 6, RULE_assignment = 7, RULE_assign_expr = 8, 
		RULE_inclusion = 9, RULE_extraction = 10, RULE_scope_stm = 11, RULE_if_stm = 12, 
		RULE_condition_block = 13, RULE_while_loop = 14, RULE_for_loop = 15, RULE_function_call_stm = 16, 
		RULE_function_call = 17, RULE_param = 18, RULE_cast = 19, RULE_type_check = 20, 
		RULE_keyword = 21, RULE_expr = 22, RULE_set_expr = 23, RULE_number_expr = 24, 
		RULE_string_expr = 25, RULE_bool_expr = 26, RULE_set = 27, RULE_element = 28, 
		RULE_tuple_element = 29, RULE_set_cardinality = 30, RULE_id = 31, RULE_reference = 32, 
		RULE_atom = 33, RULE_number_atom = 34, RULE_string_atom = 35, RULE_bool_atom = 36, 
		RULE_log = 37, RULE_input = 38;
	public static final String[] ruleNames = {
		"parse", "func_definition", "arg", "struct_element", "block", "command", 
		"stm", "assignment", "assign_expr", "inclusion", "extraction", "scope_stm", 
		"if_stm", "condition_block", "while_loop", "for_loop", "function_call_stm", 
		"function_call", "param", "cast", "type_check", "keyword", "expr", "set_expr", 
		"number_expr", "string_expr", "bool_expr", "set", "element", "tuple_element", 
		"set_cardinality", "id", "reference", "atom", "number_atom", "string_atom", 
		"bool_atom", "log", "input"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "','", "'or'", "'and'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", 
		"'+'", "'-'", "'*'", "'/'", "'!'", "'->'", "'<-'", "'++'", "'union'", 
		"'inter'", "'diff'", "'sdiff'", "'mult'", "'card'", "'&'", "';'", "'='", 
		"'('", "')'", "'{'", "'}'", "'['", "']'", "'true'", "'false'", "'if'", 
		"'else'", "'elif'", "'while'", "'for'", "'break'", "'continue'", "'return'", 
		"'log'", "'inp'", "'extern'", "'def'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "OR", "AND", "EQ", "NEQ", "GT", "LT", "GTEQ", "LTEQ", "PLUS", 
		"MINUS", "MULT", "DIV", "NOT", "INCLUDE", "EXTRACT", "CONCAT", "UNION", 
		"INTER", "DIFF", "SDIFF", "PROD", "CARD", "REF", "SCOL", "ASSIGN", "OPAR", 
		"CPAR", "OBRACE", "CBRACE", "SQLBRACE", "SQRBRACE", "TRUE", "FALSE", "IF", 
		"ELSE", "ELIF", "WHILE", "FOR", "BREAK", "CONTINUE", "RETURN", "LOG", 
		"INPUT", "EXTERNAL", "DEFINE", "CAST_TYPE", "CHECK_TYPE", "ID", "INT", 
		"FLOAT", "STRING", "COMMENT", "SPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SetGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SetGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ParseContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SetGrammarParser.EOF, 0); }
		public List<Func_definitionContext> func_definition() {
			return getRuleContexts(Func_definitionContext.class);
		}
		public Func_definitionContext func_definition(int i) {
			return getRuleContext(Func_definitionContext.class,i);
		}
		public List<Struct_elementContext> struct_element() {
			return getRuleContexts(Struct_elementContext.class);
		}
		public Struct_elementContext struct_element(int i) {
			return getRuleContext(Struct_elementContext.class,i);
		}
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DEFINE) {
				{
				{
				setState(78);
				func_definition();
				}
				}
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(87);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << OPAR) | (1L << OBRACE) | (1L << SQLBRACE) | (1L << IF) | (1L << WHILE) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << LOG) | (1L << INPUT) | (1L << EXTERNAL) | (1L << CAST_TYPE) | (1L << ID))) != 0)) {
				{
				{
				setState(84);
				struct_element();
				}
				}
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(90);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Func_definitionContext extends ParserRuleContext {
		public TerminalNode DEFINE() { return getToken(SetGrammarParser.DEFINE, 0); }
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<ArgContext> arg() {
			return getRuleContexts(ArgContext.class);
		}
		public ArgContext arg(int i) {
			return getRuleContext(ArgContext.class,i);
		}
		public Func_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_func_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterFunc_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitFunc_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitFunc_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Func_definitionContext func_definition() throws RecognitionException {
		Func_definitionContext _localctx = new Func_definitionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_func_definition);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(DEFINE);
			setState(93);
			match(ID);
			setState(94);
			match(OPAR);
			setState(108);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				{
				setState(98); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(95);
						arg();
						setState(96);
						match(T__0);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(100); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(102);
				arg();
				}
				}
				break;
			case 2:
				{
				}
				break;
			case 3:
				{
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==REF || _la==ID) {
					{
					setState(105);
					arg();
					}
				}

				}
				break;
			}
			setState(110);
			match(CPAR);
			setState(111);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgContext extends ParserRuleContext {
		public ReferenceContext reference() {
			return getRuleContext(ReferenceContext.class,0);
		}
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public ArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitArg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgContext arg() throws RecognitionException {
		ArgContext _localctx = new ArgContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_arg);
		try {
			setState(115);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case REF:
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				reference();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Struct_elementContext extends ParserRuleContext {
		public StmContext stm() {
			return getRuleContext(StmContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public Struct_elementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStruct_element(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStruct_element(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStruct_element(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Struct_elementContext struct_element() throws RecognitionException {
		Struct_elementContext _localctx = new Struct_elementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_struct_element);
		try {
			setState(120);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LT:
			case OPAR:
			case SQLBRACE:
			case IF:
			case WHILE:
			case FOR:
			case LOG:
			case INPUT:
			case EXTERNAL:
			case CAST_TYPE:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(117);
				stm();
				}
				break;
			case OBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(118);
				block();
				}
				break;
			case BREAK:
			case CONTINUE:
			case RETURN:
				enterOuterAlt(_localctx, 3);
				{
				setState(119);
				command();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode OBRACE() { return getToken(SetGrammarParser.OBRACE, 0); }
		public TerminalNode CBRACE() { return getToken(SetGrammarParser.CBRACE, 0); }
		public List<Struct_elementContext> struct_element() {
			return getRuleContexts(Struct_elementContext.class);
		}
		public Struct_elementContext struct_element(int i) {
			return getRuleContext(Struct_elementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(122);
			match(OBRACE);
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << OPAR) | (1L << OBRACE) | (1L << SQLBRACE) | (1L << IF) | (1L << WHILE) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << LOG) | (1L << INPUT) | (1L << EXTERNAL) | (1L << CAST_TYPE) | (1L << ID))) != 0)) {
				{
				{
				setState(123);
				struct_element();
				}
				}
				setState(128);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(129);
			match(CBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_command);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			keyword();
			setState(132);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmContext extends ParserRuleContext {
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public If_stmContext if_stm() {
			return getRuleContext(If_stmContext.class,0);
		}
		public While_loopContext while_loop() {
			return getRuleContext(While_loopContext.class,0);
		}
		public For_loopContext for_loop() {
			return getRuleContext(For_loopContext.class,0);
		}
		public LogContext log() {
			return getRuleContext(LogContext.class,0);
		}
		public InputContext input() {
			return getRuleContext(InputContext.class,0);
		}
		public InclusionContext inclusion() {
			return getRuleContext(InclusionContext.class,0);
		}
		public ExtractionContext extraction() {
			return getRuleContext(ExtractionContext.class,0);
		}
		public Scope_stmContext scope_stm() {
			return getRuleContext(Scope_stmContext.class,0);
		}
		public Function_call_stmContext function_call_stm() {
			return getRuleContext(Function_call_stmContext.class,0);
		}
		public StmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmContext stm() throws RecognitionException {
		StmContext _localctx = new StmContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_stm);
		try {
			setState(144);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(134);
				assignment();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(135);
				if_stm();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(136);
				while_loop();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(137);
				for_loop();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(138);
				log();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(139);
				input();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(140);
				inclusion();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(141);
				extraction();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(142);
				scope_stm();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(143);
				function_call_stm();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
	 
		public AssignmentContext() { }
		public void copyFrom(AssignmentContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TupleElementAssignmentContext extends AssignmentContext {
		public Tuple_elementContext tuple_element() {
			return getRuleContext(Tuple_elementContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SetGrammarParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public TupleElementAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterTupleElementAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitTupleElementAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitTupleElementAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GeneralAssignmentContext extends AssignmentContext {
		public Assign_exprContext assign_expr() {
			return getRuleContext(Assign_exprContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public GeneralAssignmentContext(AssignmentContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterGeneralAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitGeneralAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitGeneralAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_assignment);
		try {
			setState(154);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new GeneralAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(146);
				assign_expr();
				setState(147);
				match(SCOL);
				}
				break;
			case 2:
				_localctx = new TupleElementAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(149);
				tuple_element();
				setState(150);
				match(ASSIGN);
				setState(151);
				expr();
				setState(152);
				match(SCOL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_exprContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public TerminalNode ASSIGN() { return getToken(SetGrammarParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Assign_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterAssign_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitAssign_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitAssign_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_exprContext assign_expr() throws RecognitionException {
		Assign_exprContext _localctx = new Assign_exprContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_assign_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(156);
			match(ID);
			setState(157);
			match(ASSIGN);
			setState(158);
			expr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InclusionContext extends ParserRuleContext {
		public Set_exprContext set_expr() {
			return getRuleContext(Set_exprContext.class,0);
		}
		public TerminalNode INCLUDE() { return getToken(SetGrammarParser.INCLUDE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public InclusionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterInclusion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitInclusion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitInclusion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InclusionContext inclusion() throws RecognitionException {
		InclusionContext _localctx = new InclusionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_inclusion);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			set_expr(0);
			setState(161);
			match(INCLUDE);
			setState(162);
			expr();
			setState(163);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtractionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public TerminalNode EXTRACT() { return getToken(SetGrammarParser.EXTRACT, 0); }
		public Set_exprContext set_expr() {
			return getRuleContext(Set_exprContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public ExtractionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extraction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterExtraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitExtraction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitExtraction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtractionContext extraction() throws RecognitionException {
		ExtractionContext _localctx = new ExtractionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_extraction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			match(ID);
			setState(166);
			match(EXTRACT);
			setState(167);
			set_expr(0);
			setState(168);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Scope_stmContext extends ParserRuleContext {
		public TerminalNode EXTERNAL() { return getToken(SetGrammarParser.EXTERNAL, 0); }
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public Scope_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scope_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterScope_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitScope_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitScope_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Scope_stmContext scope_stm() throws RecognitionException {
		Scope_stmContext _localctx = new Scope_stmContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_scope_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			match(EXTERNAL);
			setState(171);
			match(ID);
			setState(172);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_stmContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(SetGrammarParser.IF, 0); }
		public List<Condition_blockContext> condition_block() {
			return getRuleContexts(Condition_blockContext.class);
		}
		public Condition_blockContext condition_block(int i) {
			return getRuleContext(Condition_blockContext.class,i);
		}
		public List<TerminalNode> ELIF() { return getTokens(SetGrammarParser.ELIF); }
		public TerminalNode ELIF(int i) {
			return getToken(SetGrammarParser.ELIF, i);
		}
		public TerminalNode ELSE() { return getToken(SetGrammarParser.ELSE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public If_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterIf_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitIf_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitIf_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_stmContext if_stm() throws RecognitionException {
		If_stmContext _localctx = new If_stmContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_if_stm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			match(IF);
			setState(175);
			condition_block();
			setState(180);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELIF) {
				{
				{
				setState(176);
				match(ELIF);
				setState(177);
				condition_block();
				}
				}
				setState(182);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(185);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(183);
				match(ELSE);
				setState(184);
				block();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Condition_blockContext extends ParserRuleContext {
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public Condition_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterCondition_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitCondition_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitCondition_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Condition_blockContext condition_block() throws RecognitionException {
		Condition_blockContext _localctx = new Condition_blockContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_condition_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			match(OPAR);
			setState(188);
			bool_expr(0);
			setState(189);
			match(CPAR);
			setState(190);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_loopContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(SetGrammarParser.WHILE, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public While_loopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_loop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterWhile_loop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitWhile_loop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitWhile_loop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_loopContext while_loop() throws RecognitionException {
		While_loopContext _localctx = new While_loopContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_while_loop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(WHILE);
			setState(193);
			match(OPAR);
			setState(194);
			bool_expr(0);
			setState(195);
			match(CPAR);
			setState(196);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_loopContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(SetGrammarParser.FOR, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public List<Assign_exprContext> assign_expr() {
			return getRuleContexts(Assign_exprContext.class);
		}
		public Assign_exprContext assign_expr(int i) {
			return getRuleContext(Assign_exprContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SetGrammarParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SetGrammarParser.SCOL, i);
		}
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public For_loopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_loop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterFor_loop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitFor_loop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitFor_loop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_loopContext for_loop() throws RecognitionException {
		For_loopContext _localctx = new For_loopContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_for_loop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(FOR);
			setState(199);
			match(OPAR);
			setState(200);
			assign_expr();
			setState(201);
			match(SCOL);
			setState(202);
			bool_expr(0);
			setState(203);
			match(SCOL);
			setState(204);
			assign_expr();
			setState(205);
			match(CPAR);
			setState(206);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_call_stmContext extends ParserRuleContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public Function_call_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterFunction_call_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitFunction_call_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitFunction_call_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_call_stmContext function_call_stm() throws RecognitionException {
		Function_call_stmContext _localctx = new Function_call_stmContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_function_call_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			function_call();
			setState(209);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_callContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public List<ParamContext> param() {
			return getRuleContexts(ParamContext.class);
		}
		public ParamContext param(int i) {
			return getRuleContext(ParamContext.class,i);
		}
		public Function_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterFunction_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitFunction_call(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitFunction_call(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_callContext function_call() throws RecognitionException {
		Function_callContext _localctx = new Function_callContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_function_call);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(211);
			match(ID);
			setState(212);
			match(OPAR);
			setState(226);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				{
				setState(216); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(213);
						param();
						setState(214);
						match(T__0);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(218); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(220);
				param();
				}
				}
				break;
			case 2:
				{
				}
				break;
			case 3:
				{
				setState(224);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << MINUS) | (1L << NOT) | (1L << CARD) | (1L << OPAR) | (1L << SQLBRACE) | (1L << TRUE) | (1L << FALSE) | (1L << CAST_TYPE) | (1L << CHECK_TYPE) | (1L << ID) | (1L << INT) | (1L << FLOAT) | (1L << STRING))) != 0)) {
					{
					setState(223);
					param();
					}
				}

				}
				break;
			}
			setState(228);
			match(CPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterParam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitParam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitParam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamContext param() throws RecognitionException {
		ParamContext _localctx = new ParamContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_param);
		try {
			setState(232);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(230);
				id();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(231);
				expr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastContext extends ParserRuleContext {
		public TerminalNode CAST_TYPE() { return getToken(SetGrammarParser.CAST_TYPE, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public CastContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cast; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterCast(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitCast(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitCast(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CastContext cast() throws RecognitionException {
		CastContext _localctx = new CastContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_cast);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			match(CAST_TYPE);
			setState(235);
			match(OPAR);
			setState(236);
			expr();
			setState(237);
			match(CPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_checkContext extends ParserRuleContext {
		public TerminalNode CHECK_TYPE() { return getToken(SetGrammarParser.CHECK_TYPE, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public Type_checkContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_check; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterType_check(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitType_check(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitType_check(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_checkContext type_check() throws RecognitionException {
		Type_checkContext _localctx = new Type_checkContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_type_check);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			match(CHECK_TYPE);
			setState(240);
			match(OPAR);
			setState(241);
			expr();
			setState(242);
			match(CPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
	 
		public KeywordContext() { }
		public void copyFrom(KeywordContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReturnKeyWordContext extends KeywordContext {
		public TerminalNode RETURN() { return getToken(SetGrammarParser.RETURN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ReturnKeyWordContext(KeywordContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterReturnKeyWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitReturnKeyWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitReturnKeyWord(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BreakKeyWordContext extends KeywordContext {
		public TerminalNode BREAK() { return getToken(SetGrammarParser.BREAK, 0); }
		public BreakKeyWordContext(KeywordContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBreakKeyWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBreakKeyWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBreakKeyWord(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContinueKeyWordContext extends KeywordContext {
		public TerminalNode CONTINUE() { return getToken(SetGrammarParser.CONTINUE, 0); }
		public ContinueKeyWordContext(KeywordContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterContinueKeyWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitContinueKeyWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitContinueKeyWord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_keyword);
		int _la;
		try {
			setState(250);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BREAK:
				_localctx = new BreakKeyWordContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(244);
				match(BREAK);
				}
				break;
			case CONTINUE:
				_localctx = new ContinueKeyWordContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(245);
				match(CONTINUE);
				}
				break;
			case RETURN:
				_localctx = new ReturnKeyWordContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(246);
				match(RETURN);
				setState(248);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << MINUS) | (1L << NOT) | (1L << CARD) | (1L << OPAR) | (1L << SQLBRACE) | (1L << TRUE) | (1L << FALSE) | (1L << CAST_TYPE) | (1L << CHECK_TYPE) | (1L << ID) | (1L << INT) | (1L << FLOAT) | (1L << STRING))) != 0)) {
					{
					setState(247);
					expr();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Set_exprContext set_expr() {
			return getRuleContext(Set_exprContext.class,0);
		}
		public Number_exprContext number_expr() {
			return getRuleContext(Number_exprContext.class,0);
		}
		public String_exprContext string_expr() {
			return getRuleContext(String_exprContext.class,0);
		}
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_expr);
		try {
			setState(256);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(252);
				set_expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(253);
				number_expr(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(254);
				string_expr(0);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(255);
				bool_expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Set_exprContext extends ParserRuleContext {
		public Set_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_set_expr; }
	 
		public Set_exprContext() { }
		public void copyFrom(Set_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SetCastExprContext extends Set_exprContext {
		public CastContext cast() {
			return getRuleContext(CastContext.class,0);
		}
		public SetCastExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetCastExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetCastExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetCastExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetAsReturnValueContext extends Set_exprContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public SetAsReturnValueContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetAsReturnValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetAsReturnValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetAsReturnValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetIdExprContext extends Set_exprContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public SetIdExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetAtomExprContext extends Set_exprContext {
		public SetContext set() {
			return getRuleContext(SetContext.class,0);
		}
		public SetAtomExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetAtomExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetAtomExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetAtomExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProdExprContext extends Set_exprContext {
		public List<Set_exprContext> set_expr() {
			return getRuleContexts(Set_exprContext.class);
		}
		public Set_exprContext set_expr(int i) {
			return getRuleContext(Set_exprContext.class,i);
		}
		public TerminalNode PROD() { return getToken(SetGrammarParser.PROD, 0); }
		public ProdExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterProdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitProdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitProdExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetAsElementExprContext extends Set_exprContext {
		public Tuple_elementContext tuple_element() {
			return getRuleContext(Tuple_elementContext.class,0);
		}
		public SetAsElementExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetAsElementExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetAsElementExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetAsElementExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetBracketExprContext extends Set_exprContext {
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Set_exprContext set_expr() {
			return getRuleContext(Set_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public SetBracketExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetBracketExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetBracketExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetBracketExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetBaseOpExprContext extends Set_exprContext {
		public Token op;
		public List<Set_exprContext> set_expr() {
			return getRuleContexts(Set_exprContext.class);
		}
		public Set_exprContext set_expr(int i) {
			return getRuleContext(Set_exprContext.class,i);
		}
		public TerminalNode UNION() { return getToken(SetGrammarParser.UNION, 0); }
		public TerminalNode INTER() { return getToken(SetGrammarParser.INTER, 0); }
		public TerminalNode DIFF() { return getToken(SetGrammarParser.DIFF, 0); }
		public TerminalNode SDIFF() { return getToken(SetGrammarParser.SDIFF, 0); }
		public SetBaseOpExprContext(Set_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetBaseOpExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetBaseOpExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetBaseOpExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Set_exprContext set_expr() throws RecognitionException {
		return set_expr(0);
	}

	private Set_exprContext set_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Set_exprContext _localctx = new Set_exprContext(_ctx, _parentState);
		Set_exprContext _prevctx = _localctx;
		int _startState = 46;
		enterRecursionRule(_localctx, 46, RULE_set_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				_localctx = new SetBracketExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(259);
				match(OPAR);
				setState(260);
				set_expr(0);
				setState(261);
				match(CPAR);
				}
				break;
			case 2:
				{
				_localctx = new SetAtomExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(263);
				set();
				}
				break;
			case 3:
				{
				_localctx = new SetAsReturnValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(264);
				function_call();
				}
				break;
			case 4:
				{
				_localctx = new SetCastExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(265);
				cast();
				}
				break;
			case 5:
				{
				_localctx = new SetAsElementExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(266);
				tuple_element();
				}
				break;
			case 6:
				{
				_localctx = new SetIdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(267);
				id();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(278);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(276);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
					case 1:
						{
						_localctx = new SetBaseOpExprContext(new Set_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_set_expr);
						setState(270);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(271);
						((SetBaseOpExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << UNION) | (1L << INTER) | (1L << DIFF) | (1L << SDIFF))) != 0)) ) {
							((SetBaseOpExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(272);
						set_expr(9);
						}
						break;
					case 2:
						{
						_localctx = new ProdExprContext(new Set_exprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_set_expr);
						setState(273);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(274);
						match(PROD);
						setState(275);
						set_expr(7);
						}
						break;
					}
					} 
				}
				setState(280);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Number_exprContext extends ParserRuleContext {
		public Number_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_expr; }
	 
		public Number_exprContext() { }
		public void copyFrom(Number_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SetCardinalityContext extends Number_exprContext {
		public Set_cardinalityContext set_cardinality() {
			return getRuleContext(Set_cardinalityContext.class,0);
		}
		public SetCardinalityContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSetCardinality(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSetCardinality(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSetCardinality(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberCastExprContext extends Number_exprContext {
		public CastContext cast() {
			return getRuleContext(CastContext.class,0);
		}
		public NumberCastExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberCastExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberCastExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberCastExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryMinusExprContext extends Number_exprContext {
		public TerminalNode MINUS() { return getToken(SetGrammarParser.MINUS, 0); }
		public Number_exprContext number_expr() {
			return getRuleContext(Number_exprContext.class,0);
		}
		public UnaryMinusExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterUnaryMinusExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitUnaryMinusExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitUnaryMinusExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberAtomExprContext extends Number_exprContext {
		public Number_atomContext number_atom() {
			return getRuleContext(Number_atomContext.class,0);
		}
		public NumberAtomExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberAtomExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberAtomExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberAtomExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberAsElementExprContext extends Number_exprContext {
		public Tuple_elementContext tuple_element() {
			return getRuleContext(Tuple_elementContext.class,0);
		}
		public NumberAsElementExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberAsElementExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberAsElementExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberAsElementExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BaseArithmeticExprContext extends Number_exprContext {
		public Token op;
		public List<Number_exprContext> number_expr() {
			return getRuleContexts(Number_exprContext.class);
		}
		public Number_exprContext number_expr(int i) {
			return getRuleContext(Number_exprContext.class,i);
		}
		public TerminalNode PLUS() { return getToken(SetGrammarParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SetGrammarParser.MINUS, 0); }
		public TerminalNode MULT() { return getToken(SetGrammarParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(SetGrammarParser.DIV, 0); }
		public BaseArithmeticExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBaseArithmeticExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBaseArithmeticExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBaseArithmeticExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberBracketExprContext extends Number_exprContext {
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Number_exprContext number_expr() {
			return getRuleContext(Number_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public NumberBracketExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberBracketExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberBracketExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberBracketExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberAsReturnValueContext extends Number_exprContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public NumberAsReturnValueContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberAsReturnValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberAsReturnValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberAsReturnValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberIdExprContext extends Number_exprContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public NumberIdExprContext(Number_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Number_exprContext number_expr() throws RecognitionException {
		return number_expr(0);
	}

	private Number_exprContext number_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Number_exprContext _localctx = new Number_exprContext(_ctx, _parentState);
		Number_exprContext _prevctx = _localctx;
		int _startState = 48;
		enterRecursionRule(_localctx, 48, RULE_number_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(294);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
				_localctx = new UnaryMinusExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(282);
				match(MINUS);
				setState(283);
				number_expr(8);
				}
				break;
			case 2:
				{
				_localctx = new NumberBracketExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(284);
				match(OPAR);
				setState(285);
				number_expr(0);
				setState(286);
				match(CPAR);
				}
				break;
			case 3:
				{
				_localctx = new NumberAtomExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(288);
				number_atom();
				}
				break;
			case 4:
				{
				_localctx = new SetCardinalityContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(289);
				set_cardinality();
				}
				break;
			case 5:
				{
				_localctx = new NumberAsReturnValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(290);
				function_call();
				}
				break;
			case 6:
				{
				_localctx = new NumberAsElementExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(291);
				tuple_element();
				}
				break;
			case 7:
				{
				_localctx = new NumberCastExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(292);
				cast();
				}
				break;
			case 8:
				{
				_localctx = new NumberIdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(293);
				id();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(301);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new BaseArithmeticExprContext(new Number_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_number_expr);
					setState(296);
					if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
					setState(297);
					((BaseArithmeticExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << MULT) | (1L << DIV))) != 0)) ) {
						((BaseArithmeticExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(298);
					number_expr(10);
					}
					} 
				}
				setState(303);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class String_exprContext extends ParserRuleContext {
		public String_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string_expr; }
	 
		public String_exprContext() { }
		public void copyFrom(String_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StringAsReturnValueContext extends String_exprContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public StringAsReturnValueContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringAsReturnValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringAsReturnValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringAsReturnValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringCastExprContext extends String_exprContext {
		public CastContext cast() {
			return getRuleContext(CastContext.class,0);
		}
		public StringCastExprContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringCastExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringCastExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringCastExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringIdExprContext extends String_exprContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public StringIdExprContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringAtomExprContext extends String_exprContext {
		public String_atomContext string_atom() {
			return getRuleContext(String_atomContext.class,0);
		}
		public StringAtomExprContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringAtomExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringAtomExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringAtomExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringConcatExprContext extends String_exprContext {
		public List<String_exprContext> string_expr() {
			return getRuleContexts(String_exprContext.class);
		}
		public String_exprContext string_expr(int i) {
			return getRuleContext(String_exprContext.class,i);
		}
		public TerminalNode CONCAT() { return getToken(SetGrammarParser.CONCAT, 0); }
		public StringConcatExprContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringConcatExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringConcatExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringConcatExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringAsElementExprContext extends String_exprContext {
		public Tuple_elementContext tuple_element() {
			return getRuleContext(Tuple_elementContext.class,0);
		}
		public StringAsElementExprContext(String_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringAsElementExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringAsElementExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringAsElementExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final String_exprContext string_expr() throws RecognitionException {
		return string_expr(0);
	}

	private String_exprContext string_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		String_exprContext _localctx = new String_exprContext(_ctx, _parentState);
		String_exprContext _prevctx = _localctx;
		int _startState = 50;
		enterRecursionRule(_localctx, 50, RULE_string_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(310);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				_localctx = new StringAtomExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(305);
				string_atom();
				}
				break;
			case 2:
				{
				_localctx = new StringCastExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(306);
				cast();
				}
				break;
			case 3:
				{
				_localctx = new StringAsReturnValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(307);
				function_call();
				}
				break;
			case 4:
				{
				_localctx = new StringAsElementExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(308);
				tuple_element();
				}
				break;
			case 5:
				{
				_localctx = new StringIdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(309);
				id();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(317);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new StringConcatExprContext(new String_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_string_expr);
					setState(312);
					if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
					setState(313);
					match(CONCAT);
					setState(314);
					string_expr(7);
					}
					} 
				}
				setState(319);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Bool_exprContext extends ParserRuleContext {
		public Bool_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_expr; }
	 
		public Bool_exprContext() { }
		public void copyFrom(Bool_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolCastExprContext extends Bool_exprContext {
		public CastContext cast() {
			return getRuleContext(CastContext.class,0);
		}
		public BoolCastExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolCastExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolCastExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolCastExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberCompExprContext extends Bool_exprContext {
		public Token op;
		public List<Number_exprContext> number_expr() {
			return getRuleContexts(Number_exprContext.class);
		}
		public Number_exprContext number_expr(int i) {
			return getRuleContext(Number_exprContext.class,i);
		}
		public TerminalNode GT() { return getToken(SetGrammarParser.GT, 0); }
		public TerminalNode LT() { return getToken(SetGrammarParser.LT, 0); }
		public TerminalNode GTEQ() { return getToken(SetGrammarParser.GTEQ, 0); }
		public TerminalNode LTEQ() { return getToken(SetGrammarParser.LTEQ, 0); }
		public TerminalNode EQ() { return getToken(SetGrammarParser.EQ, 0); }
		public TerminalNode NEQ() { return getToken(SetGrammarParser.NEQ, 0); }
		public NumberCompExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNumberCompExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNumberCompExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNumberCompExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolNegationExprContext extends Bool_exprContext {
		public TerminalNode NOT() { return getToken(SetGrammarParser.NOT, 0); }
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public BoolNegationExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolNegationExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolNegationExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolNegationExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolBaseExprContext extends Bool_exprContext {
		public Token op;
		public List<Bool_exprContext> bool_expr() {
			return getRuleContexts(Bool_exprContext.class);
		}
		public Bool_exprContext bool_expr(int i) {
			return getRuleContext(Bool_exprContext.class,i);
		}
		public TerminalNode EQ() { return getToken(SetGrammarParser.EQ, 0); }
		public TerminalNode NEQ() { return getToken(SetGrammarParser.NEQ, 0); }
		public TerminalNode AND() { return getToken(SetGrammarParser.AND, 0); }
		public TerminalNode OR() { return getToken(SetGrammarParser.OR, 0); }
		public BoolBaseExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolBaseExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolBaseExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolBaseExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolIdExprContext extends Bool_exprContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public BoolIdExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolBracketExprContext extends Bool_exprContext {
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Bool_exprContext bool_expr() {
			return getRuleContext(Bool_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public BoolBracketExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolBracketExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolBracketExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolBracketExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAtomExprContext extends Bool_exprContext {
		public Bool_atomContext bool_atom() {
			return getRuleContext(Bool_atomContext.class,0);
		}
		public BoolAtomExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolAtomExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolAtomExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolAtomExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAsReturnValueContext extends Bool_exprContext {
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public BoolAsReturnValueContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolAsReturnValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolAsReturnValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolAsReturnValue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolTypeCheckContext extends Bool_exprContext {
		public Type_checkContext type_check() {
			return getRuleContext(Type_checkContext.class,0);
		}
		public BoolTypeCheckContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolTypeCheck(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolTypeCheck(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolTypeCheck(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolAsElementExprContext extends Bool_exprContext {
		public Tuple_elementContext tuple_element() {
			return getRuleContext(Tuple_elementContext.class,0);
		}
		public BoolAsElementExprContext(Bool_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolAsElementExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolAsElementExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolAsElementExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bool_exprContext bool_expr() throws RecognitionException {
		return bool_expr(0);
	}

	private Bool_exprContext bool_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Bool_exprContext _localctx = new Bool_exprContext(_ctx, _parentState);
		Bool_exprContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_bool_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(337);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				_localctx = new BoolNegationExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(321);
				match(NOT);
				setState(322);
				bool_expr(9);
				}
				break;
			case 2:
				{
				_localctx = new BoolBracketExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(323);
				match(OPAR);
				setState(324);
				bool_expr(0);
				setState(325);
				match(CPAR);
				}
				break;
			case 3:
				{
				_localctx = new NumberCompExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(327);
				number_expr(0);
				setState(328);
				((NumberCompExprContext)_localctx).op = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NEQ) | (1L << GT) | (1L << LT) | (1L << GTEQ) | (1L << LTEQ))) != 0)) ) {
					((NumberCompExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(329);
				number_expr(0);
				}
				break;
			case 4:
				{
				_localctx = new BoolAtomExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(331);
				bool_atom();
				}
				break;
			case 5:
				{
				_localctx = new BoolCastExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(332);
				cast();
				}
				break;
			case 6:
				{
				_localctx = new BoolTypeCheckContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(333);
				type_check();
				}
				break;
			case 7:
				{
				_localctx = new BoolAsReturnValueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(334);
				function_call();
				}
				break;
			case 8:
				{
				_localctx = new BoolAsElementExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(335);
				tuple_element();
				}
				break;
			case 9:
				{
				_localctx = new BoolIdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(336);
				id();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(344);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new BoolBaseExprContext(new Bool_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_bool_expr);
					setState(339);
					if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
					setState(340);
					((BoolBaseExprContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OR) | (1L << AND) | (1L << EQ) | (1L << NEQ))) != 0)) ) {
						((BoolBaseExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(341);
					bool_expr(11);
					}
					} 
				}
				setState(346);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SetContext extends ParserRuleContext {
		public SetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_set; }
	 
		public SetContext() { }
		public void copyFrom(SetContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EmptySetContext extends SetContext {
		public TerminalNode SQLBRACE() { return getToken(SetGrammarParser.SQLBRACE, 0); }
		public TerminalNode SQRBRACE() { return getToken(SetGrammarParser.SQRBRACE, 0); }
		public EmptySetContext(SetContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterEmptySet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitEmptySet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitEmptySet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NonEmptyTupleContext extends SetContext {
		public TerminalNode LT() { return getToken(SetGrammarParser.LT, 0); }
		public TerminalNode GT() { return getToken(SetGrammarParser.GT, 0); }
		public List<ElementContext> element() {
			return getRuleContexts(ElementContext.class);
		}
		public ElementContext element(int i) {
			return getRuleContext(ElementContext.class,i);
		}
		public NonEmptyTupleContext(SetContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNonEmptyTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNonEmptyTuple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNonEmptyTuple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotEmptySetContext extends SetContext {
		public TerminalNode SQLBRACE() { return getToken(SetGrammarParser.SQLBRACE, 0); }
		public TerminalNode SQRBRACE() { return getToken(SetGrammarParser.SQRBRACE, 0); }
		public List<ElementContext> element() {
			return getRuleContexts(ElementContext.class);
		}
		public ElementContext element(int i) {
			return getRuleContext(ElementContext.class,i);
		}
		public NotEmptySetContext(SetContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterNotEmptySet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitNotEmptySet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitNotEmptySet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EmptyTupleContext extends SetContext {
		public TerminalNode LT() { return getToken(SetGrammarParser.LT, 0); }
		public TerminalNode GT() { return getToken(SetGrammarParser.GT, 0); }
		public EmptyTupleContext(SetContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterEmptyTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitEmptyTuple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitEmptyTuple(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetContext set() throws RecognitionException {
		SetContext _localctx = new SetContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_set);
		try {
			int _alt;
			setState(377);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				_localctx = new NotEmptySetContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(347);
				match(SQLBRACE);
				{
				setState(353);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						{
						setState(348);
						element();
						}
						setState(349);
						match(T__0);
						}
						} 
					}
					setState(355);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				}
				{
				setState(356);
				element();
				}
				}
				setState(358);
				match(SQRBRACE);
				}
				break;
			case 2:
				_localctx = new NonEmptyTupleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(360);
				match(LT);
				{
				setState(366);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						{
						setState(361);
						element();
						}
						setState(362);
						match(T__0);
						}
						} 
					}
					setState(368);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				}
				{
				setState(369);
				element();
				}
				}
				setState(371);
				match(GT);
				}
				break;
			case 3:
				_localctx = new EmptyTupleContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(373);
				match(LT);
				setState(374);
				match(GT);
				}
				break;
			case 4:
				_localctx = new EmptySetContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(375);
				match(SQLBRACE);
				setState(376);
				match(SQRBRACE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementContext extends ParserRuleContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public SetContext set() {
			return getRuleContext(SetContext.class,0);
		}
		public ElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElementContext element() throws RecognitionException {
		ElementContext _localctx = new ElementContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_element);
		try {
			setState(381);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
			case FALSE:
			case INT:
			case FLOAT:
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(379);
				atom();
				}
				break;
			case LT:
			case SQLBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(380);
				set();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Tuple_elementContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public Number_exprContext number_expr() {
			return getRuleContext(Number_exprContext.class,0);
		}
		public Tuple_elementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tuple_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterTuple_element(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitTuple_element(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitTuple_element(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Tuple_elementContext tuple_element() throws RecognitionException {
		Tuple_elementContext _localctx = new Tuple_elementContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_tuple_element);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			id();
			setState(384);
			match(SQLBRACE);
			setState(385);
			number_expr(0);
			setState(386);
			match(SQRBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Set_cardinalityContext extends ParserRuleContext {
		public TerminalNode CARD() { return getToken(SetGrammarParser.CARD, 0); }
		public TerminalNode OPAR() { return getToken(SetGrammarParser.OPAR, 0); }
		public Set_exprContext set_expr() {
			return getRuleContext(Set_exprContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(SetGrammarParser.CPAR, 0); }
		public Set_cardinalityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_set_cardinality; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterSet_cardinality(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitSet_cardinality(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitSet_cardinality(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Set_cardinalityContext set_cardinality() throws RecognitionException {
		Set_cardinalityContext _localctx = new Set_cardinalityContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_set_cardinality);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(388);
			match(CARD);
			setState(389);
			match(OPAR);
			setState(390);
			set_expr(0);
			setState(391);
			match(CPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(393);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReferenceContext extends ParserRuleContext {
		public TerminalNode REF() { return getToken(SetGrammarParser.REF, 0); }
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public ReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReferenceContext reference() throws RecognitionException {
		ReferenceContext _localctx = new ReferenceContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_reference);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(395);
			match(REF);
			setState(396);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public Number_atomContext number_atom() {
			return getRuleContext(Number_atomContext.class,0);
		}
		public String_atomContext string_atom() {
			return getRuleContext(String_atomContext.class,0);
		}
		public Bool_atomContext bool_atom() {
			return getRuleContext(Bool_atomContext.class,0);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_atom);
		try {
			setState(401);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
			case FLOAT:
				enterOuterAlt(_localctx, 1);
				{
				setState(398);
				number_atom();
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(399);
				string_atom();
				}
				break;
			case TRUE:
			case FALSE:
				enterOuterAlt(_localctx, 3);
				{
				setState(400);
				bool_atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Number_atomContext extends ParserRuleContext {
		public Number_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_atom; }
	 
		public Number_atomContext() { }
		public void copyFrom(Number_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FloatAtomContext extends Number_atomContext {
		public TerminalNode FLOAT() { return getToken(SetGrammarParser.FLOAT, 0); }
		public FloatAtomContext(Number_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterFloatAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitFloatAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitFloatAtom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntAtomContext extends Number_atomContext {
		public TerminalNode INT() { return getToken(SetGrammarParser.INT, 0); }
		public IntAtomContext(Number_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterIntAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitIntAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitIntAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Number_atomContext number_atom() throws RecognitionException {
		Number_atomContext _localctx = new Number_atomContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_number_atom);
		try {
			setState(405);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new IntAtomContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(403);
				match(INT);
				}
				break;
			case FLOAT:
				_localctx = new FloatAtomContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(404);
				match(FLOAT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class String_atomContext extends ParserRuleContext {
		public String_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string_atom; }
	 
		public String_atomContext() { }
		public void copyFrom(String_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StringAtomContext extends String_atomContext {
		public TerminalNode STRING() { return getToken(SetGrammarParser.STRING, 0); }
		public StringAtomContext(String_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterStringAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitStringAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitStringAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final String_atomContext string_atom() throws RecognitionException {
		String_atomContext _localctx = new String_atomContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_string_atom);
		try {
			_localctx = new StringAtomContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(407);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bool_atomContext extends ParserRuleContext {
		public Bool_atomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_atom; }
	 
		public Bool_atomContext() { }
		public void copyFrom(Bool_atomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BoolAtomContext extends Bool_atomContext {
		public TerminalNode TRUE() { return getToken(SetGrammarParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(SetGrammarParser.FALSE, 0); }
		public BoolAtomContext(Bool_atomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterBoolAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitBoolAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitBoolAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bool_atomContext bool_atom() throws RecognitionException {
		Bool_atomContext _localctx = new Bool_atomContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_bool_atom);
		int _la;
		try {
			_localctx = new BoolAtomContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(409);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogContext extends ParserRuleContext {
		public TerminalNode LOG() { return getToken(SetGrammarParser.LOG, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public LogContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_log; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterLog(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitLog(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitLog(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogContext log() throws RecognitionException {
		LogContext _localctx = new LogContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_log);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(411);
			match(LOG);
			setState(412);
			expr();
			setState(413);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InputContext extends ParserRuleContext {
		public TerminalNode INPUT() { return getToken(SetGrammarParser.INPUT, 0); }
		public TerminalNode SCOL() { return getToken(SetGrammarParser.SCOL, 0); }
		public TerminalNode ID() { return getToken(SetGrammarParser.ID, 0); }
		public InputContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_input; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).enterInput(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SetGrammarListener ) ((SetGrammarListener)listener).exitInput(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SetGrammarVisitor ) return ((SetGrammarVisitor<? extends T>)visitor).visitInput(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InputContext input() throws RecognitionException {
		InputContext _localctx = new InputContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_input);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(415);
			match(INPUT);
			setState(417);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(416);
				match(ID);
				}
			}

			setState(419);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 23:
			return set_expr_sempred((Set_exprContext)_localctx, predIndex);
		case 24:
			return number_expr_sempred((Number_exprContext)_localctx, predIndex);
		case 25:
			return string_expr_sempred((String_exprContext)_localctx, predIndex);
		case 26:
			return bool_expr_sempred((Bool_exprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean set_expr_sempred(Set_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 8);
		case 1:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean number_expr_sempred(Number_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 9);
		}
		return true;
	}
	private boolean string_expr_sempred(String_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean bool_expr_sempred(Bool_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 10);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\38\u01a8\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\3\2\7\2R\n\2\f\2\16"+
		"\2U\13\2\3\2\7\2X\n\2\f\2\16\2[\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\6"+
		"\3e\n\3\r\3\16\3f\3\3\3\3\3\3\3\3\5\3m\n\3\5\3o\n\3\3\3\3\3\3\3\3\4\3"+
		"\4\5\4v\n\4\3\5\3\5\3\5\5\5{\n\5\3\6\3\6\7\6\177\n\6\f\6\16\6\u0082\13"+
		"\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0093"+
		"\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u009d\n\t\3\n\3\n\3\n\3\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16"+
		"\3\16\7\16\u00b5\n\16\f\16\16\16\u00b8\13\16\3\16\3\16\5\16\u00bc\n\16"+
		"\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23"+
		"\3\23\6\23\u00db\n\23\r\23\16\23\u00dc\3\23\3\23\3\23\3\23\5\23\u00e3"+
		"\n\23\5\23\u00e5\n\23\3\23\3\23\3\24\3\24\5\24\u00eb\n\24\3\25\3\25\3"+
		"\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\5\27\u00fb"+
		"\n\27\5\27\u00fd\n\27\3\30\3\30\3\30\3\30\5\30\u0103\n\30\3\31\3\31\3"+
		"\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u010f\n\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\7\31\u0117\n\31\f\31\16\31\u011a\13\31\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u0129\n\32\3\32"+
		"\3\32\3\32\7\32\u012e\n\32\f\32\16\32\u0131\13\32\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\5\33\u0139\n\33\3\33\3\33\3\33\7\33\u013e\n\33\f\33\16\33\u0141"+
		"\13\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\5\34\u0154\n\34\3\34\3\34\3\34\7\34\u0159\n\34\f"+
		"\34\16\34\u015c\13\34\3\35\3\35\3\35\3\35\7\35\u0162\n\35\f\35\16\35\u0165"+
		"\13\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\7\35\u016f\n\35\f\35\16"+
		"\35\u0172\13\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u017c\n\35"+
		"\3\36\3\36\5\36\u0180\n\36\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3!"+
		"\3!\3\"\3\"\3\"\3#\3#\3#\5#\u0194\n#\3$\3$\5$\u0198\n$\3%\3%\3&\3&\3\'"+
		"\3\'\3\'\3\'\3(\3(\5(\u01a4\n(\3(\3(\3(\2\6\60\62\64\66)\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLN\2\7\3\2\24\27"+
		"\3\2\f\17\3\2\6\13\3\2\4\7\3\2#$\2\u01c8\2S\3\2\2\2\4^\3\2\2\2\6u\3\2"+
		"\2\2\bz\3\2\2\2\n|\3\2\2\2\f\u0085\3\2\2\2\16\u0092\3\2\2\2\20\u009c\3"+
		"\2\2\2\22\u009e\3\2\2\2\24\u00a2\3\2\2\2\26\u00a7\3\2\2\2\30\u00ac\3\2"+
		"\2\2\32\u00b0\3\2\2\2\34\u00bd\3\2\2\2\36\u00c2\3\2\2\2 \u00c8\3\2\2\2"+
		"\"\u00d2\3\2\2\2$\u00d5\3\2\2\2&\u00ea\3\2\2\2(\u00ec\3\2\2\2*\u00f1\3"+
		"\2\2\2,\u00fc\3\2\2\2.\u0102\3\2\2\2\60\u010e\3\2\2\2\62\u0128\3\2\2\2"+
		"\64\u0138\3\2\2\2\66\u0153\3\2\2\28\u017b\3\2\2\2:\u017f\3\2\2\2<\u0181"+
		"\3\2\2\2>\u0186\3\2\2\2@\u018b\3\2\2\2B\u018d\3\2\2\2D\u0193\3\2\2\2F"+
		"\u0197\3\2\2\2H\u0199\3\2\2\2J\u019b\3\2\2\2L\u019d\3\2\2\2N\u01a1\3\2"+
		"\2\2PR\5\4\3\2QP\3\2\2\2RU\3\2\2\2SQ\3\2\2\2ST\3\2\2\2TY\3\2\2\2US\3\2"+
		"\2\2VX\5\b\5\2WV\3\2\2\2X[\3\2\2\2YW\3\2\2\2YZ\3\2\2\2Z\\\3\2\2\2[Y\3"+
		"\2\2\2\\]\7\2\2\3]\3\3\2\2\2^_\7\60\2\2_`\7\63\2\2`n\7\35\2\2ab\5\6\4"+
		"\2bc\7\3\2\2ce\3\2\2\2da\3\2\2\2ef\3\2\2\2fd\3\2\2\2fg\3\2\2\2gh\3\2\2"+
		"\2hi\5\6\4\2io\3\2\2\2jo\3\2\2\2km\5\6\4\2lk\3\2\2\2lm\3\2\2\2mo\3\2\2"+
		"\2nd\3\2\2\2nj\3\2\2\2nl\3\2\2\2op\3\2\2\2pq\7\36\2\2qr\5\n\6\2r\5\3\2"+
		"\2\2sv\5B\"\2tv\7\63\2\2us\3\2\2\2ut\3\2\2\2v\7\3\2\2\2w{\5\16\b\2x{\5"+
		"\n\6\2y{\5\f\7\2zw\3\2\2\2zx\3\2\2\2zy\3\2\2\2{\t\3\2\2\2|\u0080\7\37"+
		"\2\2}\177\5\b\5\2~}\3\2\2\2\177\u0082\3\2\2\2\u0080~\3\2\2\2\u0080\u0081"+
		"\3\2\2\2\u0081\u0083\3\2\2\2\u0082\u0080\3\2\2\2\u0083\u0084\7 \2\2\u0084"+
		"\13\3\2\2\2\u0085\u0086\5,\27\2\u0086\u0087\7\33\2\2\u0087\r\3\2\2\2\u0088"+
		"\u0093\5\20\t\2\u0089\u0093\5\32\16\2\u008a\u0093\5\36\20\2\u008b\u0093"+
		"\5 \21\2\u008c\u0093\5L\'\2\u008d\u0093\5N(\2\u008e\u0093\5\24\13\2\u008f"+
		"\u0093\5\26\f\2\u0090\u0093\5\30\r\2\u0091\u0093\5\"\22\2\u0092\u0088"+
		"\3\2\2\2\u0092\u0089\3\2\2\2\u0092\u008a\3\2\2\2\u0092\u008b\3\2\2\2\u0092"+
		"\u008c\3\2\2\2\u0092\u008d\3\2\2\2\u0092\u008e\3\2\2\2\u0092\u008f\3\2"+
		"\2\2\u0092\u0090\3\2\2\2\u0092\u0091\3\2\2\2\u0093\17\3\2\2\2\u0094\u0095"+
		"\5\22\n\2\u0095\u0096\7\33\2\2\u0096\u009d\3\2\2\2\u0097\u0098\5<\37\2"+
		"\u0098\u0099\7\34\2\2\u0099\u009a\5.\30\2\u009a\u009b\7\33\2\2\u009b\u009d"+
		"\3\2\2\2\u009c\u0094\3\2\2\2\u009c\u0097\3\2\2\2\u009d\21\3\2\2\2\u009e"+
		"\u009f\7\63\2\2\u009f\u00a0\7\34\2\2\u00a0\u00a1\5.\30\2\u00a1\23\3\2"+
		"\2\2\u00a2\u00a3\5\60\31\2\u00a3\u00a4\7\21\2\2\u00a4\u00a5\5.\30\2\u00a5"+
		"\u00a6\7\33\2\2\u00a6\25\3\2\2\2\u00a7\u00a8\7\63\2\2\u00a8\u00a9\7\22"+
		"\2\2\u00a9\u00aa\5\60\31\2\u00aa\u00ab\7\33\2\2\u00ab\27\3\2\2\2\u00ac"+
		"\u00ad\7/\2\2\u00ad\u00ae\7\63\2\2\u00ae\u00af\7\33\2\2\u00af\31\3\2\2"+
		"\2\u00b0\u00b1\7%\2\2\u00b1\u00b6\5\34\17\2\u00b2\u00b3\7\'\2\2\u00b3"+
		"\u00b5\5\34\17\2\u00b4\u00b2\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4\3"+
		"\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00bb\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b9"+
		"\u00ba\7&\2\2\u00ba\u00bc\5\n\6\2\u00bb\u00b9\3\2\2\2\u00bb\u00bc\3\2"+
		"\2\2\u00bc\33\3\2\2\2\u00bd\u00be\7\35\2\2\u00be\u00bf\5\66\34\2\u00bf"+
		"\u00c0\7\36\2\2\u00c0\u00c1\5\n\6\2\u00c1\35\3\2\2\2\u00c2\u00c3\7(\2"+
		"\2\u00c3\u00c4\7\35\2\2\u00c4\u00c5\5\66\34\2\u00c5\u00c6\7\36\2\2\u00c6"+
		"\u00c7\5\n\6\2\u00c7\37\3\2\2\2\u00c8\u00c9\7)\2\2\u00c9\u00ca\7\35\2"+
		"\2\u00ca\u00cb\5\22\n\2\u00cb\u00cc\7\33\2\2\u00cc\u00cd\5\66\34\2\u00cd"+
		"\u00ce\7\33\2\2\u00ce\u00cf\5\22\n\2\u00cf\u00d0\7\36\2\2\u00d0\u00d1"+
		"\5\n\6\2\u00d1!\3\2\2\2\u00d2\u00d3\5$\23\2\u00d3\u00d4\7\33\2\2\u00d4"+
		"#\3\2\2\2\u00d5\u00d6\7\63\2\2\u00d6\u00e4\7\35\2\2\u00d7\u00d8\5&\24"+
		"\2\u00d8\u00d9\7\3\2\2\u00d9\u00db\3\2\2\2\u00da\u00d7\3\2\2\2\u00db\u00dc"+
		"\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de\3\2\2\2\u00de"+
		"\u00df\5&\24\2\u00df\u00e5\3\2\2\2\u00e0\u00e5\3\2\2\2\u00e1\u00e3\5&"+
		"\24\2\u00e2\u00e1\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e5\3\2\2\2\u00e4"+
		"\u00da\3\2\2\2\u00e4\u00e0\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e5\u00e6\3\2"+
		"\2\2\u00e6\u00e7\7\36\2\2\u00e7%\3\2\2\2\u00e8\u00eb\5@!\2\u00e9\u00eb"+
		"\5.\30\2\u00ea\u00e8\3\2\2\2\u00ea\u00e9\3\2\2\2\u00eb\'\3\2\2\2\u00ec"+
		"\u00ed\7\61\2\2\u00ed\u00ee\7\35\2\2\u00ee\u00ef\5.\30\2\u00ef\u00f0\7"+
		"\36\2\2\u00f0)\3\2\2\2\u00f1\u00f2\7\62\2\2\u00f2\u00f3\7\35\2\2\u00f3"+
		"\u00f4\5.\30\2\u00f4\u00f5\7\36\2\2\u00f5+\3\2\2\2\u00f6\u00fd\7*\2\2"+
		"\u00f7\u00fd\7+\2\2\u00f8\u00fa\7,\2\2\u00f9\u00fb\5.\30\2\u00fa\u00f9"+
		"\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fd\3\2\2\2\u00fc\u00f6\3\2\2\2\u00fc"+
		"\u00f7\3\2\2\2\u00fc\u00f8\3\2\2\2\u00fd-\3\2\2\2\u00fe\u0103\5\60\31"+
		"\2\u00ff\u0103\5\62\32\2\u0100\u0103\5\64\33\2\u0101\u0103\5\66\34\2\u0102"+
		"\u00fe\3\2\2\2\u0102\u00ff\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0101\3\2"+
		"\2\2\u0103/\3\2\2\2\u0104\u0105\b\31\1\2\u0105\u0106\7\35\2\2\u0106\u0107"+
		"\5\60\31\2\u0107\u0108\7\36\2\2\u0108\u010f\3\2\2\2\u0109\u010f\58\35"+
		"\2\u010a\u010f\5$\23\2\u010b\u010f\5(\25\2\u010c\u010f\5<\37\2\u010d\u010f"+
		"\5@!\2\u010e\u0104\3\2\2\2\u010e\u0109\3\2\2\2\u010e\u010a\3\2\2\2\u010e"+
		"\u010b\3\2\2\2\u010e\u010c\3\2\2\2\u010e\u010d\3\2\2\2\u010f\u0118\3\2"+
		"\2\2\u0110\u0111\f\n\2\2\u0111\u0112\t\2\2\2\u0112\u0117\5\60\31\13\u0113"+
		"\u0114\f\b\2\2\u0114\u0115\7\30\2\2\u0115\u0117\5\60\31\t\u0116\u0110"+
		"\3\2\2\2\u0116\u0113\3\2\2\2\u0117\u011a\3\2\2\2\u0118\u0116\3\2\2\2\u0118"+
		"\u0119\3\2\2\2\u0119\61\3\2\2\2\u011a\u0118\3\2\2\2\u011b\u011c\b\32\1"+
		"\2\u011c\u011d\7\r\2\2\u011d\u0129\5\62\32\n\u011e\u011f\7\35\2\2\u011f"+
		"\u0120\5\62\32\2\u0120\u0121\7\36\2\2\u0121\u0129\3\2\2\2\u0122\u0129"+
		"\5F$\2\u0123\u0129\5> \2\u0124\u0129\5$\23\2\u0125\u0129\5<\37\2\u0126"+
		"\u0129\5(\25\2\u0127\u0129\5@!\2\u0128\u011b\3\2\2\2\u0128\u011e\3\2\2"+
		"\2\u0128\u0122\3\2\2\2\u0128\u0123\3\2\2\2\u0128\u0124\3\2\2\2\u0128\u0125"+
		"\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0127\3\2\2\2\u0129\u012f\3\2\2\2\u012a"+
		"\u012b\f\13\2\2\u012b\u012c\t\3\2\2\u012c\u012e\5\62\32\f\u012d\u012a"+
		"\3\2\2\2\u012e\u0131\3\2\2\2\u012f\u012d\3\2\2\2\u012f\u0130\3\2\2\2\u0130"+
		"\63\3\2\2\2\u0131\u012f\3\2\2\2\u0132\u0133\b\33\1\2\u0133\u0139\5H%\2"+
		"\u0134\u0139\5(\25\2\u0135\u0139\5$\23\2\u0136\u0139\5<\37\2\u0137\u0139"+
		"\5@!\2\u0138\u0132\3\2\2\2\u0138\u0134\3\2\2\2\u0138\u0135\3\2\2\2\u0138"+
		"\u0136\3\2\2\2\u0138\u0137\3\2\2\2\u0139\u013f\3\2\2\2\u013a\u013b\f\b"+
		"\2\2\u013b\u013c\7\23\2\2\u013c\u013e\5\64\33\t\u013d\u013a\3\2\2\2\u013e"+
		"\u0141\3\2\2\2\u013f\u013d\3\2\2\2\u013f\u0140\3\2\2\2\u0140\65\3\2\2"+
		"\2\u0141\u013f\3\2\2\2\u0142\u0143\b\34\1\2\u0143\u0144\7\20\2\2\u0144"+
		"\u0154\5\66\34\13\u0145\u0146\7\35\2\2\u0146\u0147\5\66\34\2\u0147\u0148"+
		"\7\36\2\2\u0148\u0154\3\2\2\2\u0149\u014a\5\62\32\2\u014a\u014b\t\4\2"+
		"\2\u014b\u014c\5\62\32\2\u014c\u0154\3\2\2\2\u014d\u0154\5J&\2\u014e\u0154"+
		"\5(\25\2\u014f\u0154\5*\26\2\u0150\u0154\5$\23\2\u0151\u0154\5<\37\2\u0152"+
		"\u0154\5@!\2\u0153\u0142\3\2\2\2\u0153\u0145\3\2\2\2\u0153\u0149\3\2\2"+
		"\2\u0153\u014d\3\2\2\2\u0153\u014e\3\2\2\2\u0153\u014f\3\2\2\2\u0153\u0150"+
		"\3\2\2\2\u0153\u0151\3\2\2\2\u0153\u0152\3\2\2\2\u0154\u015a\3\2\2\2\u0155"+
		"\u0156\f\f\2\2\u0156\u0157\t\5\2\2\u0157\u0159\5\66\34\r\u0158\u0155\3"+
		"\2\2\2\u0159\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b\3\2\2\2\u015b"+
		"\67\3\2\2\2\u015c\u015a\3\2\2\2\u015d\u0163\7!\2\2\u015e\u015f\5:\36\2"+
		"\u015f\u0160\7\3\2\2\u0160\u0162\3\2\2\2\u0161\u015e\3\2\2\2\u0162\u0165"+
		"\3\2\2\2\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166\3\2\2\2\u0165"+
		"\u0163\3\2\2\2\u0166\u0167\5:\36\2\u0167\u0168\3\2\2\2\u0168\u0169\7\""+
		"\2\2\u0169\u017c\3\2\2\2\u016a\u0170\7\t\2\2\u016b\u016c\5:\36\2\u016c"+
		"\u016d\7\3\2\2\u016d\u016f\3\2\2\2\u016e\u016b\3\2\2\2\u016f\u0172\3\2"+
		"\2\2\u0170\u016e\3\2\2\2\u0170\u0171\3\2\2\2\u0171\u0173\3\2\2\2\u0172"+
		"\u0170\3\2\2\2\u0173\u0174\5:\36\2\u0174\u0175\3\2\2\2\u0175\u0176\7\b"+
		"\2\2\u0176\u017c\3\2\2\2\u0177\u0178\7\t\2\2\u0178\u017c\7\b\2\2\u0179"+
		"\u017a\7!\2\2\u017a\u017c\7\"\2\2\u017b\u015d\3\2\2\2\u017b\u016a\3\2"+
		"\2\2\u017b\u0177\3\2\2\2\u017b\u0179\3\2\2\2\u017c9\3\2\2\2\u017d\u0180"+
		"\5D#\2\u017e\u0180\58\35\2\u017f\u017d\3\2\2\2\u017f\u017e\3\2\2\2\u0180"+
		";\3\2\2\2\u0181\u0182\5@!\2\u0182\u0183\7!\2\2\u0183\u0184\5\62\32\2\u0184"+
		"\u0185\7\"\2\2\u0185=\3\2\2\2\u0186\u0187\7\31\2\2\u0187\u0188\7\35\2"+
		"\2\u0188\u0189\5\60\31\2\u0189\u018a\7\36\2\2\u018a?\3\2\2\2\u018b\u018c"+
		"\7\63\2\2\u018cA\3\2\2\2\u018d\u018e\7\32\2\2\u018e\u018f\7\63\2\2\u018f"+
		"C\3\2\2\2\u0190\u0194\5F$\2\u0191\u0194\5H%\2\u0192\u0194\5J&\2\u0193"+
		"\u0190\3\2\2\2\u0193\u0191\3\2\2\2\u0193\u0192\3\2\2\2\u0194E\3\2\2\2"+
		"\u0195\u0198\7\64\2\2\u0196\u0198\7\65\2\2\u0197\u0195\3\2\2\2\u0197\u0196"+
		"\3\2\2\2\u0198G\3\2\2\2\u0199\u019a\7\66\2\2\u019aI\3\2\2\2\u019b\u019c"+
		"\t\6\2\2\u019cK\3\2\2\2\u019d\u019e\7-\2\2\u019e\u019f\5.\30\2\u019f\u01a0"+
		"\7\33\2\2\u01a0M\3\2\2\2\u01a1\u01a3\7.\2\2\u01a2\u01a4\7\63\2\2\u01a3"+
		"\u01a2\3\2\2\2\u01a3\u01a4\3\2\2\2\u01a4\u01a5\3\2\2\2\u01a5\u01a6\7\33"+
		"\2\2\u01a6O\3\2\2\2%SYflnuz\u0080\u0092\u009c\u00b6\u00bb\u00dc\u00e2"+
		"\u00e4\u00ea\u00fa\u00fc\u0102\u010e\u0116\u0118\u0128\u012f\u0138\u013f"+
		"\u0153\u015a\u0163\u0170\u017b\u017f\u0193\u0197\u01a3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}