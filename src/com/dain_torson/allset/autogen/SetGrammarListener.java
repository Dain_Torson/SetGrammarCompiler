// Generated from resources/SetGrammar.g4 by ANTLR 4.7

package com.dain_torson.allset.autogen;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SetGrammarParser}.
 */
public interface SetGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(SetGrammarParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(SetGrammarParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#func_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunc_definition(SetGrammarParser.Func_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#func_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunc_definition(SetGrammarParser.Func_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#arg}.
	 * @param ctx the parse tree
	 */
	void enterArg(SetGrammarParser.ArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#arg}.
	 * @param ctx the parse tree
	 */
	void exitArg(SetGrammarParser.ArgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#struct_element}.
	 * @param ctx the parse tree
	 */
	void enterStruct_element(SetGrammarParser.Struct_elementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#struct_element}.
	 * @param ctx the parse tree
	 */
	void exitStruct_element(SetGrammarParser.Struct_elementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(SetGrammarParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(SetGrammarParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(SetGrammarParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(SetGrammarParser.CommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#stm}.
	 * @param ctx the parse tree
	 */
	void enterStm(SetGrammarParser.StmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#stm}.
	 * @param ctx the parse tree
	 */
	void exitStm(SetGrammarParser.StmContext ctx);
	/**
	 * Enter a parse tree produced by the {@code generalAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterGeneralAssignment(SetGrammarParser.GeneralAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code generalAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitGeneralAssignment(SetGrammarParser.GeneralAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tupleElementAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterTupleElementAssignment(SetGrammarParser.TupleElementAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupleElementAssignment}
	 * labeled alternative in {@link SetGrammarParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitTupleElementAssignment(SetGrammarParser.TupleElementAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#assign_expr}.
	 * @param ctx the parse tree
	 */
	void enterAssign_expr(SetGrammarParser.Assign_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#assign_expr}.
	 * @param ctx the parse tree
	 */
	void exitAssign_expr(SetGrammarParser.Assign_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#inclusion}.
	 * @param ctx the parse tree
	 */
	void enterInclusion(SetGrammarParser.InclusionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#inclusion}.
	 * @param ctx the parse tree
	 */
	void exitInclusion(SetGrammarParser.InclusionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#extraction}.
	 * @param ctx the parse tree
	 */
	void enterExtraction(SetGrammarParser.ExtractionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#extraction}.
	 * @param ctx the parse tree
	 */
	void exitExtraction(SetGrammarParser.ExtractionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#scope_stm}.
	 * @param ctx the parse tree
	 */
	void enterScope_stm(SetGrammarParser.Scope_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#scope_stm}.
	 * @param ctx the parse tree
	 */
	void exitScope_stm(SetGrammarParser.Scope_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#if_stm}.
	 * @param ctx the parse tree
	 */
	void enterIf_stm(SetGrammarParser.If_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#if_stm}.
	 * @param ctx the parse tree
	 */
	void exitIf_stm(SetGrammarParser.If_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#condition_block}.
	 * @param ctx the parse tree
	 */
	void enterCondition_block(SetGrammarParser.Condition_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#condition_block}.
	 * @param ctx the parse tree
	 */
	void exitCondition_block(SetGrammarParser.Condition_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#while_loop}.
	 * @param ctx the parse tree
	 */
	void enterWhile_loop(SetGrammarParser.While_loopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#while_loop}.
	 * @param ctx the parse tree
	 */
	void exitWhile_loop(SetGrammarParser.While_loopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#for_loop}.
	 * @param ctx the parse tree
	 */
	void enterFor_loop(SetGrammarParser.For_loopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#for_loop}.
	 * @param ctx the parse tree
	 */
	void exitFor_loop(SetGrammarParser.For_loopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#function_call_stm}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call_stm(SetGrammarParser.Function_call_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#function_call_stm}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call_stm(SetGrammarParser.Function_call_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(SetGrammarParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(SetGrammarParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#param}.
	 * @param ctx the parse tree
	 */
	void enterParam(SetGrammarParser.ParamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#param}.
	 * @param ctx the parse tree
	 */
	void exitParam(SetGrammarParser.ParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#cast}.
	 * @param ctx the parse tree
	 */
	void enterCast(SetGrammarParser.CastContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#cast}.
	 * @param ctx the parse tree
	 */
	void exitCast(SetGrammarParser.CastContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#type_check}.
	 * @param ctx the parse tree
	 */
	void enterType_check(SetGrammarParser.Type_checkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#type_check}.
	 * @param ctx the parse tree
	 */
	void exitType_check(SetGrammarParser.Type_checkContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterBreakKeyWord(SetGrammarParser.BreakKeyWordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitBreakKeyWord(SetGrammarParser.BreakKeyWordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterContinueKeyWord(SetGrammarParser.ContinueKeyWordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitContinueKeyWord(SetGrammarParser.ContinueKeyWordContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterReturnKeyWord(SetGrammarParser.ReturnKeyWordContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnKeyWord}
	 * labeled alternative in {@link SetGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitReturnKeyWord(SetGrammarParser.ReturnKeyWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(SetGrammarParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(SetGrammarParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setCastExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetCastExpr(SetGrammarParser.SetCastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setCastExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetCastExpr(SetGrammarParser.SetCastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetAsReturnValue(SetGrammarParser.SetAsReturnValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetAsReturnValue(SetGrammarParser.SetAsReturnValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setIdExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetIdExpr(SetGrammarParser.SetIdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setIdExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetIdExpr(SetGrammarParser.SetIdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetAtomExpr(SetGrammarParser.SetAtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetAtomExpr(SetGrammarParser.SetAtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prodExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterProdExpr(SetGrammarParser.ProdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prodExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitProdExpr(SetGrammarParser.ProdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetAsElementExpr(SetGrammarParser.SetAsElementExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetAsElementExpr(SetGrammarParser.SetAsElementExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetBracketExpr(SetGrammarParser.SetBracketExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetBracketExpr(SetGrammarParser.SetBracketExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setBaseOpExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetBaseOpExpr(SetGrammarParser.SetBaseOpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setBaseOpExpr}
	 * labeled alternative in {@link SetGrammarParser#set_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetBaseOpExpr(SetGrammarParser.SetBaseOpExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setCardinality}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterSetCardinality(SetGrammarParser.SetCardinalityContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setCardinality}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitSetCardinality(SetGrammarParser.SetCardinalityContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberCastExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberCastExpr(SetGrammarParser.NumberCastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberCastExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberCastExpr(SetGrammarParser.NumberCastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpr(SetGrammarParser.UnaryMinusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpr(SetGrammarParser.UnaryMinusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberAtomExpr(SetGrammarParser.NumberAtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberAtomExpr(SetGrammarParser.NumberAtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberAsElementExpr(SetGrammarParser.NumberAsElementExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberAsElementExpr(SetGrammarParser.NumberAsElementExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code baseArithmeticExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterBaseArithmeticExpr(SetGrammarParser.BaseArithmeticExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseArithmeticExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitBaseArithmeticExpr(SetGrammarParser.BaseArithmeticExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberBracketExpr(SetGrammarParser.NumberBracketExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberBracketExpr(SetGrammarParser.NumberBracketExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberAsReturnValue(SetGrammarParser.NumberAsReturnValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberAsReturnValue(SetGrammarParser.NumberAsReturnValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberIdExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberIdExpr(SetGrammarParser.NumberIdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberIdExpr}
	 * labeled alternative in {@link SetGrammarParser#number_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberIdExpr(SetGrammarParser.NumberIdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringAsReturnValue(SetGrammarParser.StringAsReturnValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringAsReturnValue(SetGrammarParser.StringAsReturnValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringCastExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringCastExpr(SetGrammarParser.StringCastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringCastExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringCastExpr(SetGrammarParser.StringCastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringIdExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringIdExpr(SetGrammarParser.StringIdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringIdExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringIdExpr(SetGrammarParser.StringIdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringAtomExpr(SetGrammarParser.StringAtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringAtomExpr(SetGrammarParser.StringAtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringConcatExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringConcatExpr(SetGrammarParser.StringConcatExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringConcatExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringConcatExpr(SetGrammarParser.StringConcatExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void enterStringAsElementExpr(SetGrammarParser.StringAsElementExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#string_expr}.
	 * @param ctx the parse tree
	 */
	void exitStringAsElementExpr(SetGrammarParser.StringAsElementExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolCastExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolCastExpr(SetGrammarParser.BoolCastExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolCastExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolCastExpr(SetGrammarParser.BoolCastExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberCompExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterNumberCompExpr(SetGrammarParser.NumberCompExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberCompExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitNumberCompExpr(SetGrammarParser.NumberCompExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolNegationExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolNegationExpr(SetGrammarParser.BoolNegationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolNegationExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolNegationExpr(SetGrammarParser.BoolNegationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolBaseExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolBaseExpr(SetGrammarParser.BoolBaseExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolBaseExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolBaseExpr(SetGrammarParser.BoolBaseExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolIdExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolIdExpr(SetGrammarParser.BoolIdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolIdExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolIdExpr(SetGrammarParser.BoolIdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolBracketExpr(SetGrammarParser.BoolBracketExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolBracketExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolBracketExpr(SetGrammarParser.BoolBracketExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolAtomExpr(SetGrammarParser.BoolAtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolAtomExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolAtomExpr(SetGrammarParser.BoolAtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolAsReturnValue(SetGrammarParser.BoolAsReturnValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolAsReturnValue}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolAsReturnValue(SetGrammarParser.BoolAsReturnValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolTypeCheck}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolTypeCheck(SetGrammarParser.BoolTypeCheckContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolTypeCheck}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolTypeCheck(SetGrammarParser.BoolTypeCheckContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolAsElementExpr(SetGrammarParser.BoolAsElementExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolAsElementExpr}
	 * labeled alternative in {@link SetGrammarParser#bool_expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolAsElementExpr(SetGrammarParser.BoolAsElementExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEmptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void enterNotEmptySet(SetGrammarParser.NotEmptySetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEmptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void exitNotEmptySet(SetGrammarParser.NotEmptySetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nonEmptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void enterNonEmptyTuple(SetGrammarParser.NonEmptyTupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nonEmptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void exitNonEmptyTuple(SetGrammarParser.NonEmptyTupleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code emptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void enterEmptyTuple(SetGrammarParser.EmptyTupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code emptyTuple}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void exitEmptyTuple(SetGrammarParser.EmptyTupleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code emptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void enterEmptySet(SetGrammarParser.EmptySetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code emptySet}
	 * labeled alternative in {@link SetGrammarParser#set}.
	 * @param ctx the parse tree
	 */
	void exitEmptySet(SetGrammarParser.EmptySetContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#element}.
	 * @param ctx the parse tree
	 */
	void enterElement(SetGrammarParser.ElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#element}.
	 * @param ctx the parse tree
	 */
	void exitElement(SetGrammarParser.ElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#tuple_element}.
	 * @param ctx the parse tree
	 */
	void enterTuple_element(SetGrammarParser.Tuple_elementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#tuple_element}.
	 * @param ctx the parse tree
	 */
	void exitTuple_element(SetGrammarParser.Tuple_elementContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#set_cardinality}.
	 * @param ctx the parse tree
	 */
	void enterSet_cardinality(SetGrammarParser.Set_cardinalityContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#set_cardinality}.
	 * @param ctx the parse tree
	 */
	void exitSet_cardinality(SetGrammarParser.Set_cardinalityContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(SetGrammarParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(SetGrammarParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(SetGrammarParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(SetGrammarParser.ReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(SetGrammarParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(SetGrammarParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 */
	void enterIntAtom(SetGrammarParser.IntAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 */
	void exitIntAtom(SetGrammarParser.IntAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code floatAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 */
	void enterFloatAtom(SetGrammarParser.FloatAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code floatAtom}
	 * labeled alternative in {@link SetGrammarParser#number_atom}.
	 * @param ctx the parse tree
	 */
	void exitFloatAtom(SetGrammarParser.FloatAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link SetGrammarParser#string_atom}.
	 * @param ctx the parse tree
	 */
	void enterStringAtom(SetGrammarParser.StringAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringAtom}
	 * labeled alternative in {@link SetGrammarParser#string_atom}.
	 * @param ctx the parse tree
	 */
	void exitStringAtom(SetGrammarParser.StringAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolAtom}
	 * labeled alternative in {@link SetGrammarParser#bool_atom}.
	 * @param ctx the parse tree
	 */
	void enterBoolAtom(SetGrammarParser.BoolAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolAtom}
	 * labeled alternative in {@link SetGrammarParser#bool_atom}.
	 * @param ctx the parse tree
	 */
	void exitBoolAtom(SetGrammarParser.BoolAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#log}.
	 * @param ctx the parse tree
	 */
	void enterLog(SetGrammarParser.LogContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#log}.
	 * @param ctx the parse tree
	 */
	void exitLog(SetGrammarParser.LogContext ctx);
	/**
	 * Enter a parse tree produced by {@link SetGrammarParser#input}.
	 * @param ctx the parse tree
	 */
	void enterInput(SetGrammarParser.InputContext ctx);
	/**
	 * Exit a parse tree produced by {@link SetGrammarParser#input}.
	 * @param ctx the parse tree
	 */
	void exitInput(SetGrammarParser.InputContext ctx);
}