package com.dain_torson.allset.visitors;

import com.dain_torson.allset.autogen.SetGrammarBaseVisitor;
import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.compiler.OutputGenerator;
import com.dain_torson.allset.compiler.Scope;
import com.dain_torson.allset.compiler.ScopeManager;
import org.stringtemplate.v4.ST;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class CompilerVisitor extends SetGrammarBaseVisitor<String> {

    private ScopeManager manager;

    public CompilerVisitor() {
        manager = new ScopeManager();
    }

    private boolean inScope(String id) {
        for(int idx = 0; idx < manager.size() - 1; ++ idx) {
            if(manager.get(idx).containsVariable(id)) {
                return true;
            }
        }
        return false;
    }

    private String correctVariableName(String idValue) {
        if(inScope(idValue) && !manager.getLast().containsExternal(idValue)) {
            return  "_" + idValue + (manager.size() - 1);
        }
        return idValue;
    }

    @Override
    public String visitBlock(SetGrammarParser.BlockContext ctx) {
        manager.add();
        ST current = OutputGenerator.getInstance().newBlockTemplate();
        ctx.struct_element().forEach(this::visit);
        OutputGenerator.getInstance().removeTemplate();
        manager.removeLast();
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitAssign_expr(SetGrammarParser.Assign_exprContext ctx) {
        String idValue = correctVariableName(ctx.ID().getText());
        ST current;
        if(manager.getLast().containsExternal(ctx.ID().getText())
                || manager.getLast().containsVariable(idValue)) {
            current = OutputGenerator.getInstance().getTemp("reassignment");
        }
        else {
            current = OutputGenerator.getInstance().getTemp("assignment");
        }

        current.add("rightValue", this.visit(ctx.expr()));
        manager.getLast().addVariable(idValue);

        current.add("id", idValue);
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitScope_stm(SetGrammarParser.Scope_stmContext ctx) {
        manager.getLast().addExternal(ctx.ID().getText());
        return "";
    }

    @Override
    public String visitIntAtom(SetGrammarParser.IntAtomContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("value_init");
        current.add("content", ctx.getText());
        return current.render();
    }

    @Override
    public String visitStringAtom(SetGrammarParser.StringAtomContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("value_init");
        current.add("content", ctx.getText());
        return current.render();
    }

    @Override
    public String visitFloatAtom(SetGrammarParser.FloatAtomContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("value_init");
        current.add("content", ctx.getText());
        return current.render();
    }

    @Override
    public String visitBoolAtom(SetGrammarParser.BoolAtomContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("value_init");
        current.add("content", ctx.getText());
        return current.render();
    }

    @Override
    public String visitId(SetGrammarParser.IdContext ctx) {
        String idValue = ctx.getText();
        String newId = "_" + idValue + (manager.size() - 1);
        if(!manager.getLast().containsExternal(idValue)) {
            if(inScope(idValue) && manager.getLast().containsVariable(newId)) {
                idValue = newId;
            }
        }
        ST template = OutputGenerator.getInstance().getTemp("value_init");
        template.add("content", idValue + ".getValue()");
        return template.render();
    }

    @Override
    public String visitLog(SetGrammarParser.LogContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("log");
        current.add("content", this.visit(ctx.expr()));
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitInput(SetGrammarParser.InputContext ctx) {
        ST current;
        if(ctx.ID() == null) {
            current = OutputGenerator.getInstance().getTemp("input");
        }
        else {
            String idValue = correctVariableName(ctx.ID().getText());
            if(manager.getLast().containsExternal(ctx.ID().getText())
                    || manager.getLast().containsVariable(idValue)) {
                current = OutputGenerator.getInstance().getTemp("input_reassign");
            }
            else {
                current = OutputGenerator.getInstance().getTemp("input_assign");
            }
            current.add("id", idValue);
            manager.getLast().addVariable(idValue);
        }
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitEmptyTuple(SetGrammarParser.EmptyTupleContext ctx) {
        return OutputGenerator.getInstance().getTemp("list").render();
    }

    @Override
    public String visitNotEmptySet(SetGrammarParser.NotEmptySetContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("set");
        for(SetGrammarParser.ElementContext context : ctx.element()) {
            template.add("elements", this.visit(context));
        }
        return template.render();
    }

    @Override
    public String visitNonEmptyTuple(SetGrammarParser.NonEmptyTupleContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("list");
        for(SetGrammarParser.ElementContext context : ctx.element()) {
            template.add("elements", this.visit(context));
        }
        return template.render();
    }

    @Override
    public String visitEmptySet(SetGrammarParser.EmptySetContext ctx) {
        return OutputGenerator.getInstance().getTemp("set").render();
    }

    @Override
    public String visitInclusion(SetGrammarParser.InclusionContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("inclusion");
        current.add("leftValue", this.visit(ctx.set_expr()));
        current.add("rightValue", this.visit(ctx.expr()));
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitExtraction(SetGrammarParser.ExtractionContext ctx) {
        ST current;
        String idValue = correctVariableName(ctx.ID().getText());
        if(manager.getLast().containsExternal(ctx.ID().getText())
                || manager.getLast().containsVariable(idValue)) {
            current = OutputGenerator.getInstance().getTemp("extraction_reassignment");
        }
        else {
            current = OutputGenerator.getInstance().getTemp("extraction");
        }
        manager.getLast().addVariable(idValue);

        current.add("id", idValue);
        current.add("rightValue", this.visit(ctx.set_expr()));
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitSetBaseOpExpr(SetGrammarParser.SetBaseOpExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("base_operation");
        template.add("lValue", this.visit(ctx.set_expr(0)));
        template.add("rValue", this.visit(ctx.set_expr(1)));
        template.add("opType", ctx.op.getType());
        return template.render();
    }

    @Override
    public String visitProdExpr(SetGrammarParser.ProdExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("product");
        template.add("lValue", this.visit(ctx.set_expr(0)));
        template.add("rValue", this.visit(ctx.set_expr(1)));
        return template.render();
    }

    @Override
    public String visitTuple_element(SetGrammarParser.Tuple_elementContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("tuple_element");
        template.add("id", this.visit(ctx.id()));
        template.add("idx", this.visit(ctx.number_expr()));
        return template.render();
    }

    @Override
    public String visitTupleElementAssignment(SetGrammarParser.TupleElementAssignmentContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("replace_tuple_element");
        template.add("id", this.visit(ctx.tuple_element().id()));
        template.add("idx", this.visit(ctx.tuple_element().number_expr()));
        template.add("newElement", this.visit(ctx.expr()));

        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", template.render());
        return "";
    }

    @Override
    public String visitSet_cardinality(SetGrammarParser.Set_cardinalityContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("cardinality");
        template.add("collection", this.visit(ctx.set_expr()));
        return template.render();
    }

    @Override
    public String visitBaseArithmeticExpr(SetGrammarParser.BaseArithmeticExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("base_number_operation");
        template.add("lValue", this.visit(ctx.number_expr(0)));
        template.add("rValue", this.visit(ctx.number_expr(1)));
        template.add("opType", ctx.op.getType());
        return template.render();
    }

    @Override
    public String visitUnaryMinusExpr(SetGrammarParser.UnaryMinusExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("unary_minus");
        template.add("target", this.visit(ctx.number_expr()));
        return template.render();
    }

    @Override
    public String visitNumberCompExpr(SetGrammarParser.NumberCompExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("comparison");
        template.add("lValue", this.visit(ctx.number_expr(0)));
        template.add("rValue", this.visit(ctx.number_expr(1)));
        template.add("opType", ctx.op.getType());
        return template.render();
    }

    @Override
    public String visitNumberBracketExpr(SetGrammarParser.NumberBracketExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("brackets");
        template.add("content", this.visit(ctx.number_expr()));
        return template.render();
    }

    @Override
    public String visitBoolBaseExpr(SetGrammarParser.BoolBaseExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("base_bool_operation");
        template.add("lValue", this.visit(ctx.bool_expr(0)));
        template.add("rValue", this.visit(ctx.bool_expr(1)));
        template.add("opType", ctx.op.getType());
        return template.render();
    }

    @Override
    public String visitBoolNegationExpr(SetGrammarParser.BoolNegationExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("negation");
        template.add("target", this.visit(ctx.bool_expr()));
        return template.render();
    }

    @Override
    public String visitBoolBracketExpr(SetGrammarParser.BoolBracketExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("brackets");
        template.add("content", this.visit(ctx.bool_expr()));
        return template.render();
    }

    @Override
    public String visitStringConcatExpr(SetGrammarParser.StringConcatExprContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("concatenation");
        template.add("lValue", this.visit(ctx.string_expr(0)));
        template.add("rValue", this.visit(ctx.string_expr(1)));
        return template.render();
    }

    @Override
    public String visitIf_stm(SetGrammarParser.If_stmContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("if_stm");

        for(int idx = 0; idx < ctx.condition_block().size(); ++idx) {
            String conditionStr = this.visit(ctx.condition_block(idx).bool_expr());

            ST condition_block = OutputGenerator.getInstance().getTemp("condition_block");
            condition_block.add("condition_expr", conditionStr);

            ST wrapper = OutputGenerator.getInstance().newWrapper();
            this.visit(ctx.condition_block(idx).block());
            condition_block.add("block", wrapper.render());
            OutputGenerator.getInstance().removeTemplate();

            current.add("condition_blocks", condition_block.render());
        }

        if(ctx.block() != null) {
            ST else_block = OutputGenerator.getInstance().getTemp("else_block");
            ST wrapper = OutputGenerator.getInstance().newWrapper();
            this.visit(ctx.block());
            else_block.add("block", wrapper.render());
            current.add("else_block", else_block.render());
            OutputGenerator.getInstance().removeTemplate();
        }

        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitWhile_loop(SetGrammarParser.While_loopContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("while_loop");
        current.add("condition_expr", this.visit(ctx.bool_expr()));

        ST wrapper = OutputGenerator.getInstance().newWrapper();
        this.visit(ctx.block());
        current.add("block", wrapper.render());
        OutputGenerator.getInstance().removeTemplate();

        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitFor_loop(SetGrammarParser.For_loopContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("for_loop");
        manager.add();

        ST wrapper = OutputGenerator.getInstance().newWrapper();
        this.visit(ctx.assign_expr(0));
        current.add("assignment", wrapper.render());
        OutputGenerator.getInstance().removeTemplate();

        String rightValue = this.visit(ctx.assign_expr(1).expr());
        ST reassignTemp = OutputGenerator.getInstance().getTemp("base_reassignment");
        reassignTemp.add("id", ctx.assign_expr(1).ID().getText());
        reassignTemp.add("rightValue", rightValue);
        String inc_expr = reassignTemp.render();
        current.add("inc_expr", inc_expr.substring(0, inc_expr.length() - 1));

        current.add("condition_expr", this.visit(ctx.bool_expr()));

        manager.setAdditionalVariables(manager.removeLast().getVariables());

        wrapper = OutputGenerator.getInstance().newWrapper();
        this.visit(ctx.block());
        current.add("block", wrapper.render());
        OutputGenerator.getInstance().removeTemplate();

        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", current.render());
        return "";
    }

    @Override
    public String visitCommand(SetGrammarParser.CommandContext ctx) {
        ST base = OutputGenerator.getInstance().getLastTemplate();
        base.add("structural_elements", this.visit(ctx.keyword()));
        return "";
    }

    @Override
    public String visitBreakKeyWord(SetGrammarParser.BreakKeyWordContext ctx) {
        return "break;";
    }

    @Override
    public String visitContinueKeyWord(SetGrammarParser.ContinueKeyWordContext ctx) {
        return "continue;";
    }

    @Override
    public String visitReturnKeyWord(SetGrammarParser.ReturnKeyWordContext ctx) {
        String result = "return ";
        if(ctx.expr() != null) {
            result += this.visit(ctx.expr());
        }
        else {
            result += "Value.VOID";
        }
        result += ";";
        manager.setReturned(true);
        return result;
    }

    @Override
    public String visitFunc_definition(SetGrammarParser.Func_definitionContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("function");
        current.add("name", ctx.ID().getText());

        List<String> additionalVariables = new ArrayList<>();
        for(SetGrammarParser.ArgContext context : ctx.arg()) {
            if(context.ID() != null) {
                current.add("args", context.ID().getText());
                current.add("configs", context.ID().getText()
                        + " = CollectionOperationsExecutor.copy("
                        + context.ID().getText() + ");");
                additionalVariables.add(context.ID().getText());
            }
            else  {
                current.add("args", context.reference().ID().getText());
                current.add("configs", context.reference().ID().getText() + ".setReference();");
                additionalVariables.add(context.reference().ID().getText() );
            }
        }
        manager.setAdditionalVariables(additionalVariables);

        ST wrapper = OutputGenerator.getInstance().newWrapper();
        this.visit(ctx.block());
        OutputGenerator.getInstance().removeTemplate();
        String body = wrapper.render();
        current.add("body", body.substring(1, body.length() - 1));
        if(!manager.isReturned()) {
            current.add("return", "return Value.VOID;");
        }

        ST base = OutputGenerator.getInstance().getBaseTemp();
        base.add("function_definitions", current.render());
        manager.clear();
        return  "";
    }

    @Override
    public String visitFunction_call(SetGrammarParser.Function_callContext ctx) {
        ST current = OutputGenerator.getInstance().getTemp("function_call");
        current.add("name", ctx.ID().getText());

        for(SetGrammarParser.ParamContext context : ctx.param()) {
            if(context.expr() != null) {
                current.add("params", this.visit(context.expr()));
            }
            else {
                current.add("params", context.id().getText());
            }
        }
        return current.render();
    }

    @Override
    public String visitStm(SetGrammarParser.StmContext ctx) {
        if(ctx.function_call_stm() != null) {
            ST base = OutputGenerator.getInstance().getLastTemplate();
            base.add("structural_elements", this.visit(ctx.function_call_stm().function_call()) + ";");
            return "";
        }
        else {
            return super.visitStm(ctx);
        }
    }

    @Override
    public String visitCast(SetGrammarParser.CastContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("cast");
        template.add("target", this.visit(ctx.expr()));
        template.add("expr", ctx.CAST_TYPE().getText());
        return template.render();
    }

    @Override
    public String visitType_check(SetGrammarParser.Type_checkContext ctx) {
        ST template = OutputGenerator.getInstance().getTemp("type_check");
        template.add("target", this.visit(ctx.expr()));
        template.add("expr", ctx.CHECK_TYPE().getText());
        return template.render();
    }
}
