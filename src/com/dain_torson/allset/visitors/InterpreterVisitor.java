package com.dain_torson.allset.visitors;

import com.dain_torson.allset.autogen.SetGrammarBaseVisitor;
import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.interpreter.data.Command;
import com.dain_torson.allset.interpreter.data.Value;
import com.dain_torson.allset.interpreter.memory.Memory;
import com.dain_torson.allset.interpreter.memory.Reference;
import com.dain_torson.allset.interpreter.operations.*;

import java.util.*;
import java.util.stream.Collectors;

public class InterpreterVisitor extends SetGrammarBaseVisitor<Value> {

    private boolean areTheSameType(Value value1, Value value2) {
        return (value1.isSet() && value2.isSet()) || (value1.isTuple() && value2.isTuple());
    }

    @Override
    public Value visitParse(SetGrammarParser.ParseContext ctx) {
        return super.visitParse(ctx);
    }

    @Override
    public Value visitBlock(SetGrammarParser.BlockContext ctx) {
        Memory.getInstance().createScope();
        for(SetGrammarParser.Struct_elementContext context : ctx.struct_element()) {
            Value value = this.visit(context);
            if(value.isCommand()) {
                Memory.getInstance().destroyScope();
                return value;
            }
        }
        Memory.getInstance().destroyScope();
        return Value.VOID;
    }

    @Override
    public Value visitAssign_expr(SetGrammarParser.Assign_exprContext ctx) {
        String id = ctx.ID().getText();
        Value value = this.visit(ctx.expr());
        if(Memory.getInstance().get(id) != null) {
            Value oldValue = Memory.getInstance().get(id);
            if(oldValue.isReference()) {
                Value refContent = Memory.getInstance().getValueByReference(oldValue.asReference());
                refContent.setValue(value.getValue());
                return value;
            }
        }
        return Memory.getInstance().put(id, value);
    }

    @Override
    public Value visitGeneralAssignment(SetGrammarParser.GeneralAssignmentContext ctx) {
        return this.visit(ctx.assign_expr());
    }

    @Override
    public Value visitScope_stm(SetGrammarParser.Scope_stmContext ctx) {
        Memory.getInstance().setExternal(ctx.ID().getText());
        return Value.VOID;
    }

    @Override
    public Value visitIntAtom(SetGrammarParser.IntAtomContext ctx) {
        return new Value(Integer.valueOf(ctx.getText()));
    }

    @Override
    public Value visitStringAtom(SetGrammarParser.StringAtomContext ctx) {
        String str = ctx.getText();
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
        return new Value(str);
    }

    @Override
    public Value visitFloatAtom(SetGrammarParser.FloatAtomContext ctx) {
        return new Value(Double.valueOf(ctx.getText()));
    }

    @Override
    public Value visitBoolAtom(SetGrammarParser.BoolAtomContext ctx) {
        return new Value(Boolean.valueOf(ctx.getText()));
    }

    @Override
    public Value visitId(SetGrammarParser.IdContext ctx) {
        String id = ctx.getText();
        Value value = Memory.getInstance().get(id);
        if(value == null) {
            throw new RuntimeException("no such variable: " + id);
        }
        if(value.isReference()) {
            value = new Value(Memory.getInstance()
                    .getValueByReference(value.asReference())
                    .getValue()
            );
        }
        return value;
    }

    @Override
    public Value visitLog(SetGrammarParser.LogContext ctx) {
        Value value = this.visit(ctx.expr());
        System.out.println(value);
        return value;
    }

    @Override
    public Value visitInput(SetGrammarParser.InputContext ctx) {
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        if(ctx.ID() != null) {
            Memory.getInstance().put(ctx.ID().getText(), new Value(input));
        }
        return new Value(input);
    }

    @Override
    public Value visitNotEmptySet(SetGrammarParser.NotEmptySetContext ctx) {
        Set<Value> set = ctx.element().stream().map(this::visit).collect(Collectors.toSet());
        return new Value(set);
    }
    @Override
    public Value visitEmptySet(SetGrammarParser.EmptySetContext ctx) {
        return new Value(new HashSet<Value>());
    }

    @Override
    public Value visitNonEmptyTuple(SetGrammarParser.NonEmptyTupleContext ctx) {
        List<Value> tuple = ctx.element().stream().map(this::visit).collect(Collectors.toList());
        return new Value(tuple);
    }

    @Override
    public Value visitEmptyTuple(SetGrammarParser.EmptyTupleContext ctx) {
        return new Value(new ArrayList<Value>());
    }

    @Override
    public Value visitInclusion(SetGrammarParser.InclusionContext ctx) {
        Value leftValue = this.visit(ctx.set_expr());
        Value rightValue = this.visit(ctx.expr());

        if(!leftValue.isCollection()) {
            throw new RuntimeException("Left value should be collection");
        }

        Collection<Value> collection;

        if(leftValue.isSet()) {
            collection = leftValue.asSet();
        }
        else {
            collection = leftValue.asTuple();
        }
        collection.add(rightValue);
        return leftValue;
    }

    @Override
    public Value visitExtraction(SetGrammarParser.ExtractionContext ctx) {
        String id = ctx.ID().getText();
        Value rightValue = this.visit(ctx.set_expr());

        if(!rightValue.isCollection()) {
            throw new RuntimeException("Right value should be collection");
        }

        Value value;
        if(rightValue.isSet()) {
            Iterator<Value> iterator = rightValue.asSet().iterator();
            if(!iterator.hasNext()) return Value.VOID;
            value = iterator.next();
            iterator.remove();
        }
        else {
            if(rightValue.asTuple().isEmpty()) return Value.VOID;
            value = rightValue.asTuple().remove(rightValue.asTuple().size() - 1);
        }
        Memory.getInstance().put(id, value);
        return value;
    }

    @Override
    public Value visitSetBaseOpExpr(SetGrammarParser.SetBaseOpExprContext ctx) {
        Value leftValue = this.visit(ctx.set_expr(0));
        Value rightValue = this.visit(ctx.set_expr(1));

        if(!leftValue.isCollection() || !rightValue.isCollection()) {
            throw new RuntimeException("Only collections allow base set operations");
        }
        if(!areTheSameType(leftValue, rightValue)) {
            throw new RuntimeException("Collections types are not the same");
        }

        BaseCollectionOperationProvider provider = BaseCollectionOperationProvider.getInstance();

        if(leftValue.isSet()) {
            return new Value(provider.provide(ctx.op.getType())
                    .apply(leftValue.asSet(), rightValue.asSet())
                    .collect(Collectors.toSet()));
        }
        else {
            return new Value(provider.provide(ctx.op.getType())
                    .apply(leftValue.asTuple(), rightValue.asTuple())
                    .collect(Collectors.toList()));
        }
    }

    @Override
    public Value visitProdExpr(SetGrammarParser.ProdExprContext ctx) {
        Value leftValue = this.visit(ctx.set_expr(0));
        Value rightValue = this.visit(ctx.set_expr(1));

        if(!leftValue.isCollection() || (leftValue.isTuple() && !areTheSameType(leftValue, rightValue))) {
            throw new RuntimeException("Only sets allow multiplication operation");
        }
            return new Value(CollectionProcessor.cartesianProduct(leftValue.asSet(), rightValue.asSet()));
    }


    @Override
    public Value visitTuple_element(SetGrammarParser.Tuple_elementContext ctx) {
        Value value = this.visit(ctx.id());
        Value idx = this.visit(ctx.number_expr());

        if(!value.isTuple()) {
            throw new RuntimeException("tuple expected");
        }

        if(!idx.isNumber()) {
            throw new RuntimeException("index is not a number");
        }
        if(!idx.isInteger()) {
            idx = new Value(idx.asDouble().intValue());
        }

        if(idx.asInteger() >= value.asTuple().size() ||
                idx.asInteger() < 0) {
            throw new RuntimeException("Tuple index out of boundary");
        }

        return value.asTuple().get(idx.asInteger());
    }

    void replaceTupleElement(SetGrammarParser.Tuple_elementContext ctx, Value newValue) {
        Value tuple = this.visit(ctx.id());
        Value idx = this.visit(ctx.number_expr());

        if(!tuple.isTuple()) {
            throw new RuntimeException("tuple expected");
        }

        if(!idx.isNumber()) {
            throw new RuntimeException("index is not a number");
        }
        if(!idx.isInteger()) {
            idx = new Value(idx.asDouble().intValue());
        }

        if(idx.asInteger() >= tuple.asTuple().size() ||
                idx.asInteger() < 0) {
            throw new RuntimeException("Tuple index out of boundary");
        }

        int targetIndex = idx.asInteger();
        tuple.asTuple().remove(targetIndex);
        tuple.asTuple().add(targetIndex, newValue);
    }

    @Override
    public Value visitTupleElementAssignment(SetGrammarParser.TupleElementAssignmentContext ctx) {
        Value value = this.visit(ctx.expr());
        replaceTupleElement(ctx.tuple_element(), value);
        return value;
    }

    @Override
    public Value visitSet_cardinality(SetGrammarParser.Set_cardinalityContext ctx) {
        Value collection = this.visit(ctx.set_expr());
        if(collection.isSet()) return new Value(collection.asSet().size());
        return new Value(collection.asTuple().size());
    }

    @Override
    public Value visitBaseArithmeticExpr(SetGrammarParser.BaseArithmeticExprContext ctx) {

        Value leftValue = this.visit(ctx.number_expr(0));
        Value rightValue = this.visit(ctx.number_expr(1));

        if(!leftValue.isNumber() || !rightValue.isNumber()) {
            throw new RuntimeException("Only numbers allow arithmetic operations");
        }

        boolean bothInt = false;

        if(leftValue.isInteger() && rightValue.isInteger()) {
            bothInt = true;
        }

        if(leftValue.isInteger()) {
            leftValue = new Value(Double.valueOf(leftValue.asInteger()));
        }
        if(rightValue.isInteger()) {
            rightValue = new Value(Double.valueOf(rightValue.asInteger()));
        }

        Value result = BaseArithmeticOperationProvider.getInstance().provide(ctx.op.getType())
                .apply(leftValue, rightValue);

        if(bothInt) {
            result = new Value(result.asDouble().intValue());
        }
        return result;
    }

    @Override
    public Value visitUnaryMinusExpr(SetGrammarParser.UnaryMinusExprContext ctx) {
        Value value = this.visit(ctx.number_expr());
        if(!value.isNumber()) {
            throw new RuntimeException("Only numbers allow arithmetic operations");
        }
        if(value.isInteger()) {
            return new Value(-value.asInteger());
        }
        else {
            return new Value(-value.asDouble());
        }
    }

    @Override
    public Value visitNumberBracketExpr(SetGrammarParser.NumberBracketExprContext ctx) {
        return this.visit(ctx.number_expr());
    }

    @Override
    public Value visitNumberCompExpr(SetGrammarParser.NumberCompExprContext ctx) {
        Value leftValue = this.visit(ctx.number_expr(0));
        Value rightValue = this.visit(ctx.number_expr(1));

        if(!leftValue.isNumber() || !rightValue.isNumber()) {
            throw new RuntimeException("Number expected.");
        }

        if(leftValue.isInteger()) {
            leftValue = new Value(Double.valueOf(leftValue.asInteger()));
        }
        if(rightValue.isInteger()) {
            rightValue = new Value(Double.valueOf(rightValue.asInteger()));
        }

        return BaseArithmeticOperationProvider.getInstance().provide(ctx.op.getType())
                .apply(leftValue, rightValue);
    }

    @Override
    public Value visitBoolBaseExpr(SetGrammarParser.BoolBaseExprContext ctx) {
        Value leftValue = this.visit(ctx.bool_expr(0));
        Value rightValue = this.visit(ctx.bool_expr(1));

        if(!leftValue.isBoolean() || !rightValue.isBoolean()) {
            throw new RuntimeException("Boolean value expected");
        }

        return BaseBoolOperationProvider.getInstance().provide(ctx.op.getType())
                .apply(leftValue, rightValue);
    }

    @Override
    public Value visitBoolNegationExpr(SetGrammarParser.BoolNegationExprContext ctx) {
        Value value = this.visit(ctx.bool_expr());
        if(!value.isBoolean()) {
            throw new RuntimeException("Boolean value expected");
        }
        return new Value(!value.asBoolean());
    }

    @Override
    public Value visitBoolBracketExpr(SetGrammarParser.BoolBracketExprContext ctx) {
        return this.visit(ctx.bool_expr());
    }

    @Override
    public Value visitStringConcatExpr(SetGrammarParser.StringConcatExprContext ctx) {
        Value leftValue = this.visit(ctx.string_expr(0));
        Value rightValue = this.visit(ctx.string_expr(1));

        if(!leftValue.isString()) {
            leftValue = new Value(leftValue.toString());
        }
        if(!rightValue.isString()) {
            rightValue = new Value(rightValue.toString());
        }
        return new Value(leftValue.asString() + rightValue.asString());
    }

    @Override
    public Value visitSetBracketExpr(SetGrammarParser.SetBracketExprContext ctx) {
        return this.visit(ctx.set_expr());
    }

    @Override
    public Value visitCondition_block(SetGrammarParser.Condition_blockContext ctx) {
        return this.visit(ctx.block());
    }

    @Override
    public Value visitIf_stm(SetGrammarParser.If_stmContext ctx) {
        List<SetGrammarParser.Condition_blockContext> conditions =  ctx.condition_block();

        boolean evaluatedBlock = false;
        Value returnValue = Value.VOID;

        for(SetGrammarParser.Condition_blockContext condition : conditions) {

            Value evaluated = this.visit(condition.bool_expr());

            if(evaluated.asBoolean()) {
                evaluatedBlock = true;
                returnValue = this.visit(condition.block());
                break;
            }
        }

        if(!evaluatedBlock && ctx.block()!= null) {
            returnValue = this.visit(ctx.block());
        }

        return returnValue;
    }

    @Override
    public Value visitWhile_loop(SetGrammarParser.While_loopContext ctx) {
        Value condition = this.visit(ctx.bool_expr());

        while(condition.asBoolean()) {
            Value value = this.visit(ctx.block());
            if(value.isCommand()) {
                if(value.asCommand().getType() != Command.Type.CONTINUE) {
                    Memory.getInstance().destroyScope();
                    return value.asCommand().getType() == Command.Type.BREAK ? Value.VOID : value;
                }
            }
            condition = this.visit(ctx.bool_expr());
        }

        return Value.VOID;
    }

    @Override
    public Value visitFor_loop(SetGrammarParser.For_loopContext ctx) {
        Memory.getInstance().createScope();
        this.visit(ctx.assign_expr(0));
        Value condition = this.visit(ctx.bool_expr());
        Memory.getInstance().forbidVarCreation();

        while(condition.asBoolean()) {
            Value value = this.visit(ctx.block());
            if(value.isCommand()) {
                if(value.asCommand().getType() != Command.Type.CONTINUE) {
                    Memory.getInstance().destroyScope();
                    return value.asCommand().getType() == Command.Type.BREAK ? Value.VOID : value;
                }
            }
            this.visit(ctx.assign_expr(1));
            condition = this.visit(ctx.bool_expr());
        }
        Memory.getInstance().destroyScope();
        return Value.VOID;

    }

    @Override
    public Value visitCommand(SetGrammarParser.CommandContext ctx) {
        return this.visit(ctx.keyword());
    }

    @Override
    public Value visitBreakKeyWord(SetGrammarParser.BreakKeyWordContext ctx) {
        return new Value(new Command(Command.Type.BREAK));
    }

    @Override
    public Value visitContinueKeyWord(SetGrammarParser.ContinueKeyWordContext ctx) {
        return new Value(new Command(Command.Type.CONTINUE));
    }

    @Override
    public Value visitReturnKeyWord(SetGrammarParser.ReturnKeyWordContext ctx) {
        if(ctx.expr() != null) {
            return new Value(new Command(this.visit(ctx.expr()), Command.Type.RETURN));
        }
        return new Value(new Command(Command.Type.RETURN));
    }

    @Override
    public Value visitFunc_definition(SetGrammarParser.Func_definitionContext ctx) {
        Memory.getInstance().newFunction(ctx.ID().getText(), ctx);
        return Value.VOID;
    }

    private boolean checkParams(SetGrammarParser.Function_callContext ctx1,
                                SetGrammarParser.Func_definitionContext ctx2) {
        if((ctx1.param() == null && ctx2.arg() != null) ||  (ctx1.param() != null && ctx2.arg() == null)) {
            return false;
        }
        return !((ctx1.param() != null && ctx2.arg() != null) && (ctx1.param().size() != ctx2.arg().size()));
    }

    public Value copy(Value collection) {
        if(collection.isSet()) {
            return new Value(collection.asSet().stream().collect(Collectors.toSet()));
        }
        else if(collection.isTuple()) {
            return  new Value(collection.asTuple().stream().collect(Collectors.toList()));
        }
        else {
            return collection;
        }
    }

    @Override
    public Value visitFunction_call(SetGrammarParser.Function_callContext ctx) {
        String name = ctx.ID().getText();
        SetGrammarParser.Func_definitionContext definitionContext = Memory.getInstance().functionCall(name);
        if(definitionContext == null) {
            throw new RuntimeException("Function " + name + " not defined");
        }
        if(!checkParams(ctx, definitionContext)) {
            throw new RuntimeException("Number of params doesn't match");
        }

        if(definitionContext.arg() != null) {
            for(int argNum  = 0; argNum < definitionContext.arg().size(); ++argNum) {
                String id;
                Value value;
                if(definitionContext.arg().get(argNum).reference() != null) {
                    if(ctx.param().get(argNum).id() == null) {
                        throw new RuntimeException("Arg " + argNum + ": Varible id expected");
                    }
                    id = definitionContext.arg().get(argNum).reference().ID().getText();
                    Reference reference = Memory.getInstance()
                            .createReference(ctx.param().get(argNum).id().ID().getText());
                    value = new Value(reference);
                }
                else {
                    id = definitionContext.arg().get(argNum).ID().getText();
                    value = this.visit(ctx.param().get(argNum));
                    if(value.isCollection()) {
                        value = copy(value);
                    }
                }
                Memory.getInstance().put(id, value);
            }
        }

        Value value = this.visit(definitionContext.block());
        Value returnValue = Value.VOID;
        if(value.isCommand()) {
            if(value.asCommand().getType() != Command.Type.RETURN) {
                throw new RuntimeException("Unexpected keyword");
            }
            if(value.asCommand().getValue() != null) {
                returnValue = value.asCommand().getValue();
            }
        }
        Memory.getInstance().functionReturn();
        return returnValue;
    }

    @Override
    public Value visitCast(SetGrammarParser.CastContext ctx) {
        return CastOperationProvider.getInstance().provide(ctx.CAST_TYPE().getText())
                .apply(this.visit(ctx.expr()));
    }

    @Override
    public Value visitType_check(SetGrammarParser.Type_checkContext ctx) {
        return TypeCheckOperationProvider.getInstance().provide(ctx.CHECK_TYPE().getText())
                .apply(this.visit(ctx.expr()));
    }
}
