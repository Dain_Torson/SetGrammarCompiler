package com.dain_torson.allset.interpreter.operations;

import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.interpreter.data.Value;

import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Stream;

public class BaseCollectionOperationProvider {

    private static BaseCollectionOperationProvider instance = new BaseCollectionOperationProvider();

    private HashMap<Integer, Function2<Collection<Value>, Stream<Value>>> functionMap;

    private BaseCollectionOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put(SetGrammarParser.UNION, CollectionProcessor::union);
        functionMap.put(SetGrammarParser.INTER, CollectionProcessor::intersection);
        functionMap.put(SetGrammarParser.DIFF, CollectionProcessor::difference);
        functionMap.put(SetGrammarParser.SDIFF, CollectionProcessor::symDifference);
    }

    public Function2<Collection<Value>, Stream<Value>> provide(Integer type) {
        return functionMap.get(type);
    }

    public static BaseCollectionOperationProvider getInstance() {
        return instance;
    }
}
