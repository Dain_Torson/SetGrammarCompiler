package com.dain_torson.allset.interpreter.operations;

import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;
import java.util.function.Function;

public class CastOperationProvider {

    private static CastOperationProvider instance = new CastOperationProvider();

    private HashMap<String, Function<Value, Value>> functionMap;

    private CastOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put("to_number", CastProcessor::castToNumber);
        functionMap.put("to_bool", CastProcessor::castToBool);
        functionMap.put("to_string", CastProcessor::castToString);
        functionMap.put("to_set", CastProcessor::castToSet);
        functionMap.put("to_tuple", CastProcessor::castToList);
    }

    public Function<Value, Value> provide(String type) {
        return functionMap.get(type);
    }

    public static CastOperationProvider getInstance() {
        return instance;
    }
}
