package com.dain_torson.allset.interpreter.operations;


import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;
import java.util.function.Function;

public class TypeCheckOperationProvider {
    private static TypeCheckOperationProvider instance = new TypeCheckOperationProvider();

    private HashMap<String, Function<Value, Value>> functionMap;

    private TypeCheckOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put("is_number", value -> new Value(value.isNumber()));
        functionMap.put("is_bool", value -> new Value(value.isBoolean()));
        functionMap.put("is_string", value -> new Value(value.isString()));
        functionMap.put("is_set", value -> new Value(value.isSet()));
        functionMap.put("is_tuple", value -> new Value(value.isTuple()));
    }

    public Function<Value, Value> provide(String type) {
        return functionMap.get(type);
    }

    public static TypeCheckOperationProvider getInstance() {
        return instance;
    }
}
