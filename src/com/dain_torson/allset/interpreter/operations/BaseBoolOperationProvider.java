package com.dain_torson.allset.interpreter.operations;


import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;

public class BaseBoolOperationProvider {

    private static BaseBoolOperationProvider instance = new BaseBoolOperationProvider();
    private HashMap<Integer, Function2<Value, Value>> functionMap;

    private BaseBoolOperationProvider() {
        functionMap = new HashMap<>();
        initialize();
    }

    private void initialize() {
        functionMap.put(SetGrammarParser.AND, (arg1, arg2) -> new Value(arg1.asBoolean() && arg2.asBoolean()));
        functionMap.put(SetGrammarParser.OR, (arg1, arg2) -> new Value(arg1.asBoolean() || arg2.asBoolean()));
        functionMap.put(SetGrammarParser.EQ, (arg1, arg2) -> new Value(arg1.asBoolean() == arg2.asBoolean()));
        functionMap.put(SetGrammarParser.NEQ, (arg1, arg2) -> new Value(arg1.asBoolean() != arg2.asBoolean()));
    }

    public Function2<Value, Value> provide(Integer type) {
        return functionMap.get(type);
    }

    public static BaseBoolOperationProvider getInstance() {
        return instance;
    }
}