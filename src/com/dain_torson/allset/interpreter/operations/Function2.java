package com.dain_torson.allset.interpreter.operations;

@FunctionalInterface
public interface Function2<T, R>{
    R apply(T t1, T t2);
}
