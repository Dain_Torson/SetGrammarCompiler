package com.dain_torson.allset.interpreter.operations;


import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;
import java.util.Objects;

public class BaseArithmeticOperationProvider {

    private static BaseArithmeticOperationProvider instance = new BaseArithmeticOperationProvider();
    private HashMap<Integer, Function2<Value, Value>> functionMap;

        private BaseArithmeticOperationProvider() {
            functionMap = new HashMap<>();
            initialize();
        }

        private void initialize() {
            functionMap.put(SetGrammarParser.PLUS, (arg1, arg2) -> new Value(arg1.asDouble() + arg2.asDouble()));
            functionMap.put(SetGrammarParser.MINUS, (arg1, arg2) -> new Value(arg1.asDouble() - arg2.asDouble()));
            functionMap.put(SetGrammarParser.MULT, (arg1, arg2) -> new Value(arg1.asDouble() * arg2.asDouble()));
            functionMap.put(SetGrammarParser.DIV, (arg1, arg2) -> new Value(arg1.asDouble() / arg2.asDouble()));
            functionMap.put(SetGrammarParser.GT, (arg1, arg2) -> new Value(arg1.asDouble() > arg2.asDouble()));
            functionMap.put(SetGrammarParser.LT, (arg1, arg2) -> new Value(arg1.asDouble() < arg2.asDouble()));
            functionMap.put(SetGrammarParser.GTEQ, (arg1, arg2) -> new Value(arg1.asDouble() >= arg2.asDouble()));
            functionMap.put(SetGrammarParser.LTEQ, (arg1, arg2) -> new Value(arg1.asDouble() <= arg2.asDouble()));
            functionMap.put(SetGrammarParser.EQ, (arg1, arg2) -> new Value(Objects.equals(arg1.asDouble(), arg2.asDouble())));
            functionMap.put(SetGrammarParser.NEQ, (arg1, arg2) -> new Value(!Objects.equals(arg1.asDouble(), arg2.asDouble())));
        }

        public Function2<Value, Value> provide(Integer type) {
            return functionMap.get(type);
        }

        public static BaseArithmeticOperationProvider getInstance() {
            return instance;
        }
}