package com.dain_torson.allset.interpreter.data;


import com.dain_torson.allset.autogen.SetGrammarParser;
import java.util.Set;

public class FunctionData {

    private SetGrammarParser.Func_definitionContext ctx;
    private Set<String> globalIds;

    public FunctionData(SetGrammarParser.Func_definitionContext ctx, Set<String> globalIds) {
        this.ctx = ctx;
        this.globalIds = globalIds;
    }

    public SetGrammarParser.Func_definitionContext getCtx() {
        return ctx;
    }

    public Set<String> getGlobalIds() {
        return globalIds;
    }
}
