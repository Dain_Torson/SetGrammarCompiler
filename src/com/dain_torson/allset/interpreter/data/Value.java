package com.dain_torson.allset.interpreter.data;

import com.dain_torson.allset.interpreter.data.Command;
import com.dain_torson.allset.interpreter.memory.Reference;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Value {

    public static Value VOID = new Value(new Object());

    private Object value;

    public Value(Object value) {
        this.value = value;
    }

    public Boolean asBoolean() {
        return (Boolean)value;
    }

    public Double asDouble() {
        return (Double)value;
    }

    public Integer asInteger() {return ((Integer) value);}

    public String asString() {
        return String.valueOf(value);
    }

    public Command asCommand() {
        return (Command) value;
    }

    public Reference asReference() {
        return (Reference) value;
    }

    public Set<Value> asSet(){return (Set<Value>) value;}

    public List<Value> asTuple() {return (List<Value>) value;}

    public boolean isDouble() {
        return value instanceof Double;
    }

    public boolean isBoolean() {return value instanceof Boolean;}

    public boolean isInteger() {return value instanceof Integer;}

    public boolean isString() {return  value instanceof String;}

    public boolean isCommand() {
        return value instanceof Command;
    }

    public boolean isReference() {
        return value instanceof Reference;
    }

    public boolean isSet() {return value instanceof Set;}

    public boolean isTuple() {return value instanceof List;}

    public boolean isCollection() {
        return isSet() || isTuple();
    }

    public boolean isNumber() {return value instanceof Number;}

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public int hashCode() {

        if(value == null) {
            return 0;
        }

        return this.value.hashCode();
    }

    @Override
    public boolean equals(Object o) {

        if(value == o) {
            return true;
        }

        if(value == null || o == null || o.getClass() != this.getClass()) {
            return false;
        }

        Value that = (Value)o;

        return this.value.equals(that.value);
    }

    @Override
    public String toString() {
        if(!isCollection()) {
            return String.valueOf(value);
        }

        Collection<Value> collection;

        String output = "";
        if(isSet()) {
            output += "[";
            collection = (Set<Value>)value;
        }
        else {
            output += "<";
            collection = (List<Value>)value;
        }

        for(Value element : collection) {
            output += element.toString();
            output += ", ";
        }

        if(output.length() > 2) {
            output = output.substring(0, output.length() - 2);
        }

        if(isSet()) output += "]";
        else  output += ">";

        return output;
    }
}