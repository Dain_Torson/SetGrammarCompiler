package com.dain_torson.allset.interpreter.data;


public class Command {

    private Type type;
    private Value value;

    public Command(Type type) {
        this.type = type;
    }

    public Command(Value value, Type type) {
        this.value = value;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public Value getValue() {
        return value;
    }

    public enum Type {RETURN, BREAK, CONTINUE}
}
