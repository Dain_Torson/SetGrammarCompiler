package com.dain_torson.allset.interpreter.memory;


public class Reference {
    public int listNumber;
    public String id;

    @Override
    public String toString() {
        return "Reference " + listNumber + ":" + id;
    }
}
