package com.dain_torson.allset.interpreter.memory;


import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;
import java.util.Set;

public class GlobalScope implements Scope{

    private HashMap<String, Value> variables;

    public GlobalScope() {
        variables = new HashMap<>();
    }

    @Override
    public Value put(String id, Value value) {
        return variables.put(id, value);
    }

    @Override
    public Value get(String id) {
        return variables.get(id);
    }

    @Override
    public boolean addExternal(String id) {
        return false;
    }

    @Override
    public boolean isExternal(String id) {
        return false;
    }

    @Override
    public Set<String> getIds() {
        return variables.keySet();
    }

    @Override
    public boolean isVarCreationAllowed(String id) {
        return true;
    }

    @Override
    public void setVarCreationLock(boolean varCreationLock) {

    }
}
