package com.dain_torson.allset.interpreter.memory;

import com.dain_torson.allset.interpreter.data.Value;

import java.util.Set;

public interface Scope {
    Value put(String id, Value value);
    Value get(String id);
    boolean addExternal(String id);
    boolean isExternal(String id);
    Set<String> getIds();
    boolean isVarCreationAllowed(String id);
    void setVarCreationLock(boolean varCreationLock);
}
