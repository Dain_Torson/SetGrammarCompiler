package com.dain_torson.allset.interpreter.memory;


import com.dain_torson.allset.autogen.SetGrammarParser;
import com.dain_torson.allset.interpreter.data.FunctionData;
import com.dain_torson.allset.interpreter.data.Value;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;

public class Memory {

    private LinkedList<Scope> currentScopeList;
    private LinkedList<LinkedList<Scope>> scopeLists;
    private HashMap<String, FunctionData> functions;
    private static Memory instance = new Memory();

    private Memory() {
        functions = new HashMap<>();
        scopeLists = new LinkedList<>();
        init();
    }

    private void init() {
        LinkedList<Scope> newList = new LinkedList<>();
        newList.add(new GlobalScope());
        currentScopeList = newList;
        scopeLists.add(newList);
    }

    private void newScopeList() {
        LinkedList<Scope> newList = new LinkedList<>();
        newList.add(scopeLists.get(0).get(0));
        newList.add(new LocalScope());
        currentScopeList = newList;
        scopeLists.add(newList);
    }

    private void deleteScopeList() {
        if(scopeLists.size() > 1) {
            scopeLists.removeLast();
            currentScopeList = scopeLists.getLast();
        }
    }

    public Reference createReference(String id) {
        Reference reference = new Reference();
        reference.id = id;
        reference.listNumber = scopeLists.size() - 2;
        return reference;
    }

    public Value getValueByReference(Reference ref) {
        for(int idx = scopeLists.get(ref.listNumber).size() - 1; idx > -1; --idx) {
            if(scopeLists.get(ref.listNumber).get(idx).get(ref.id) != null) {
                return scopeLists.get(ref.listNumber).get(idx).get(ref.id);
            }
        }
        return null;
    }

    private Set<String> getGlobalVarSnapshot() {
        Set<String> globals = scopeLists.get(0).get(0).getIds();
        return globals.stream().collect(Collectors.toSet());
    }

    public void newFunction(String id, SetGrammarParser.Func_definitionContext ctx) {
        FunctionData data = new FunctionData(ctx, getGlobalVarSnapshot());
        functions.put(id, data);
    }

    public SetGrammarParser.Func_definitionContext functionCall(String id) {
        if(functions.get(id) == null) return null;
        SetGrammarParser.Func_definitionContext ctx = functions.get(id).getCtx();
        newScopeList();
        return ctx;
    }

    public void functionReturn() {
        deleteScopeList();
    }

    public void createScope() {
        currentScopeList.add(new LocalScope());
    }

    public void destroyScope() {
        if(currentScopeList.size() > 1) {
            currentScopeList.removeLast();
        }
    }

    public void setExternal(String id) {
        currentScopeList.getLast().addExternal(id);
    }

    public void forbidVarCreation() {
        currentScopeList.getLast().setVarCreationLock(true);
    }

    public Value put(String id, Value value) {
        for(int idx = currentScopeList.size() - 1; idx > -1; --idx) {
            if(!(currentScopeList.get(idx).isExternal(id) || !currentScopeList.get(idx).isVarCreationAllowed(id))) {
                currentScopeList.get(idx).put(id, value);
                return value;
            }
        }
        return value;
    }

    public Value get(String id) {
        for(int idx = currentScopeList.size() - 1; idx > -1; --idx) {
            if(currentScopeList.get(idx).get(id) != null) {
                return currentScopeList.get(idx).get(id);
            }
        }
        return null;
    }

    public static Memory getInstance() {
        return instance;
    }
}
