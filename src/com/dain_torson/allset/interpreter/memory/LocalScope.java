package com.dain_torson.allset.interpreter.memory;

import com.dain_torson.allset.interpreter.data.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class LocalScope implements Scope{

    private HashMap<String, Value> variables;
    private List<String> externals;
    boolean varCreationLock = false;

    public LocalScope() {
        variables = new HashMap<>();
        externals = new ArrayList<>();
    }

    @Override
    public Value put(String id, Value value) {
        return variables.put(id, value);
    }

    @Override
    public Value get(String id) {
        return variables.get(id);
    }

    @Override
    public boolean addExternal(String id) {
        return externals.add(id);
    }

    @Override
    public boolean isExternal(String id) {
        return externals.contains(id);
    }

    @Override
    public Set<String> getIds() {
        return variables.keySet();
    }

    @Override
    public boolean isVarCreationAllowed(String id) {
        return !varCreationLock || variables.containsKey(id);
    }

    public void setVarCreationLock(boolean varCreationLock) {
        this.varCreationLock = varCreationLock;
    }
}
